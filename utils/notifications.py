import boto3
import application_status
import logging
import constants

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def initiate_error_notification(dst_bucket, dst_key, error):
    """This method initiates the error SNS notification.

    Args:
        dst_bucket (str): Destination bucket.
        dst_key (str): Destination FileName
        json_data (str): Processed Json data
        error (str): Error
    """
    if constants.SNS_REALTIME_PROCESSING:
        try:
            # Initialize the sns client
            sns_client = boto3.client('sns')

            if sns_client is None:
                warning = "SNS Client is offline"
                logger.warn(warning)
                initiate_service_down_notification("SNS Client", warning)
                return application_status.ERROR_RESPONSE

            logger.info("Preparing the SNS client to send notification")
            message = constants.SNS_ERROR_NOTIFICATION_MESSAGE + \
                      constants.SNS_DESTINATION_BUCKET + dst_bucket + \
                      constants.SNS_DESTINATION_KEY + dst_key + \
                      constants.SNS_ERROR_DETAILS + error
            subject = constants.SNS_ERROR_NOTIFICATION_SUBJECT
            sns_client.publish(TopicArn=constants.SNS_ERROR_TOPIC_ARN, Message=message, Subject=subject)
            # sns_client.publish(TopicArn=constants.SNS_SUPPORT_TOPIC_ARN, Message=message, Subject=subject)

            logger.info("Email Notification successfully sent")
        except Exception as e:
            raise IOError(e)
    else:
        logger.warning("SNS Notification is disabled.")


def initiate_service_down_notification(service_name, warning):
    """This method initiates the Service down SNS notification.

    Args:
        service_name (str): Service Name.
        warning (str): Warning
    """
    if constants.SNS_REALTIME_PROCESSING:
        try:
            # Initialize the sns client
            sns_client = boto3.client('sns')

            if sns_client is None:
                logger.warning("SNS Client is offline")
                return application_status.ERROR_RESPONSE

            logger.info("Preparing the SNS client to send notification")
            message = constants.SNS_SERVICE_DOWN_NOTIFICATION_MESSAGE + \
                      constants.SNS_SERVICE_NAME + service_name + \
                      constants.SNS_WARNING_DETAILS + warning
            subject = constants.SNS_SERVICE_DOWN_NOTIFICATION_SUBJECT
            sns_client.publish(TopicArn=constants.SNS_WARN_TOPIC_ARN, Message=message, Subject=subject)
            # sns_client.publish(TopicArn=constants.SNS_SUPPORT_TOPIC_ARN, Message=message, Subject=subject)

            logger.info("SNS Notification successfully sent")
        except Exception as e:
            raise IOError(e)
    else:
        logger.warning("SNS Notification is disabled.")


def initiate_empty_notification(dst_bucket, dst_key, error):
    """This method initiates the empty SNS notification.
    Emtpy -> Not available in Source

    Args:
        dst_bucket (str): Destination bucket.
        dst_key (str): Destination FileName
        error (str): Error
    """
    if constants.SNS_REALTIME_PROCESSING:
        try:
            # Initialize the sns client
            sns_client = boto3.client('sns')

            if sns_client is None:
                warning = "SNS Client is offline"
                logger.warning(warning)
                initiate_service_down_notification("SNS Client", warning)
                return application_status.ERROR_RESPONSE

            logger.info("Preparing the SNS client to send notification")
            message = constants.SNS_EMPTY_NOTIFICATION_MESSAGE + \
                      constants.SNS_DESTINATION_BUCKET + dst_bucket + \
                      constants.SNS_DESTINATION_KEY + dst_key + \
                      constants.SNS_ERROR_DETAILS + error
            subject = constants.SNS_EMPTY_NOTIFICATION_SUBJECT
            sns_client.publish(TopicArn=constants.SNS_ERROR_TOPIC_ARN, Message=message, Subject=subject)
            # sns_client.publish(TopicArn=constants.SNS_SUPPORT_TOPIC_ARN, Message=message, Subject=subject)

            logger.info("Email Notification successfully sent")
        except Exception as e:
            raise IOError(e)
    else:
        logger.warning("SNS Notification is disabled.")


def initiate_insufficient_privileges_notification(privilege_name, warning):
    """This method initiates the insufficient privileges SNS notification.

    Args:
        privilege_name (str): Privilege Name.
        warning (str): Warning
    """
    if constants.SNS_REALTIME_PROCESSING:
        try:
            # Initialize the sns client
            sns_client = boto3.client('sns')

            if sns_client is None:
                warning = "SNS Client is offline"
                logger.warning(warning)
                initiate_service_down_notification("SNS Client", warning)
                return application_status.ERROR_RESPONSE

            logger.info("Preparing the SNS client to send notification")
            message = constants.SNS_INSUFFICIENT_PRIVILEGES_NOTIFICATION_MESSAGE + \
                      constants.SNS_PRIVILEGE_NAME + privilege_name + \
                      constants.SNS_WARNING_DETAILS + warning
            subject = constants.SNS_INSUFFICIENT_PRIVILEGES_NOTIFICATION_SUBJECT
            sns_client.publish(TopicArn=constants.SNS_WARN_TOPIC_ARN, Message=message, Subject=subject)
            # sns_client.publish(TopicArn=constants.SNS_SUPPORT_TOPIC_ARN, Message=message, Subject=subject)

            logger.info("Email Notification successfully sent")
        except Exception as e:
            raise IOError(e)
    else:
        logger.warning("SNS Notification is disabled.")