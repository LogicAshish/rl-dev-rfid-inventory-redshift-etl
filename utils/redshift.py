import boto3
import time
import logging
import json
import numpy as np
import pandas as pd
import constants

logger = logging.getLogger('redshift')
logger.setLevel(logging.DEBUG)
redshift_data_client = boto3.client('redshift-data')

# DEFAULT CONFIG
default_cluster = constants.REDSHIFT_CLUSTER
default_database = constants.REDSHIFT_DATABASE
default_user = constants.REDSHIFT_USER

# CONN RETRY CONFIG
MAX_CONN_RETRIES = constants.REDSHIFT_MAX_CONN_RETRIES
CONN_WAIT_SECONDS = constants.REDSHIFT_CONN_WAIT_SECONDS


# ERROR CLASSES
class TimeoutException(Exception) : pass 
class QueryFailure(Exception) : pass


def execute_sql(sql, name="sql_statement", cluster=default_cluster, database=default_database, user=default_user):
    """
        Returns Query ID or None
    """
    params = {
        "ClusterIdentifier" : cluster,
        "Database"  : database,
        "DbUser" : user,
        "Sql" : sql,
        "StatementName" : name
    }
    logger.debug(f"Executing Query : {json.dumps(sql)}")
    resp = redshift_data_client.execute_statement(**params)
    if int(resp['ResponseMetadata']['HTTPStatusCode']) == 200 : 
        logger.info(f"QUERY NAME : {name};  QUERY ID : {resp.get('Id')}")
        return resp.get('Id')
        
    logger.error(f"Query Request Failure Response : {resp}")
    return None
    

def sql_status(query_id) :
    """
        Returns Query Status
        Values : 'ABORTED'|'ALL'|'FAILED'|'FINISHED'|'PICKED'|'STARTED'|'SUBMITTED'
    """
    resp = redshift_data_client.describe_statement(Id=query_id)
    if int(resp['ResponseMetadata']['HTTPStatusCode']) == 200 :
        return resp.get('Status')
    return None


def sql_status_details(query_id) : 
    """
        Returns query response dict
    """
    resp = redshift_data_client.describe_statement(Id=query_id)
    if int(resp['ResponseMetadata']['HTTPStatusCode']) == 200 :
        return resp
    return None


def wait_for_queries(query_ids, max_attempts=450, poll_interval=2, retry_config = None) :
    """
        Blocking Operation.
        Wait until query has finished executing or timeout.

        If query failure due to Connection Refused, attempts to retry query execution.
        
        max_attempts : number of times to poll before raising error
        poll_interval (in seconds) determines wait time between polls
        retry_config = None or
            {
                'cluster' : XYZ     [Optional] - uses default value if unspecified
                'database' : XYZ    [Optional] - uses default value if unspecified
                'user' : XYZ        [Optional] - uses default value if unspecified
            }
            specifies the parameters to pass to execute_sql() while re executing failed SQL on Connection Failure

        Returns True if all query "FINISHED"
        
        Raises Timeout Exception
        Raises QueryFailure Exceeded
    """
    pending = list(set(query_ids))
    assert not None in pending
    conn_retry_attempt = 0
    attempt = 0
    start_time = int(time.time())
    while attempt < max_attempts :
        query_pending = pending.copy()
        
        if not query_pending :
            logger.info(f"TIME ELAPSED : {int(time.time()) - start_time}")
            return True
        
        for id in query_pending :
            status = sql_status(id)
            if status is None : continue

            if status in ['ABORTED', 'FAILED'] :
                error_response = sql_status_details(id)
                logger.error(f"ABORTED/FAILED QUERY RESPONSE : {error_response}")
                if error_response is None :
                    logger.debug("Encountered None status. Skipping to next loop.")
                    continue
                Error = error_response.get('Error', f'Unable to retrieve sql error details for {id}')

                # if error due to "Connection refused", retry query.
                # Any error is assumed to be a connection error.
                # This gives us additional buffer against novel error responses
                #   In th worst case, few erroneous SQL will fail multiple times which is an acceptable tradeoff.

                # WARNING : Retry logic is ineffective when sensitive queries are used!
                # For example : COPY commands needs to specify IAM roles. Any attempt to retry this will fail
                #   because the QueryString extracted will contain empty IAM_roles.
                # Hence, we do not attempt to retry in such scenarios.
                QueryString = error_response.get('QueryString')
                is_sensitive = 'iam_role ' in QueryString.lower()
                if conn_retry_attempt < MAX_CONN_RETRIES and status == 'FAILED' and not is_sensitive:
                    conn_retry_attempt += 1
                    time.sleep(CONN_WAIT_SECONDS)
                    logger.info(f"Detected Connection Failure. Retrying query {id}")
                    if retry_config is None :
                        q = execute_sql(QueryString, name="default_conn_retry_sql")
                    else :
                        q = execute_sql(QueryString, name="configured_conn_retry_sql", **retry_config)
                    pending.remove(id)
                    pending.append(q)
                else :
                    raise QueryFailure(f"Query {id} : {status}")
            if status in ['FINISHED'] : pending.remove(id)
        attempt += 1
        time.sleep(poll_interval)
        
    raise TimeoutException("Exceeded Attempts")


def get_results(query_id):
    """
      retrieves all query results . Handles pagination.
      Returns a DataFrame (without columns)
    """
    paginator = redshift_data_client.get_paginator('get_statement_result')
    iterator = paginator.paginate(Id=query_id)
    records = []
    for data in iterator:
        for record in data['Records']:
            records.append(np.array(list(map(cast, record))))

    # create and return a dataframe
    records = np.array(records)
    df = pd.DataFrame(records)
    return df


def cast(entry):
    """
    eg entry : {'stringValue' : 'xyz'}
    """
    typ, value = list(entry.items())[0]
    if typ == 'stringValue':
        return str(value)
    elif typ == 'longValue':
        return int(value)
    elif typ == 'isNull':
        return None
    elif typ == 'booleanValue':
        return bool(value)
    elif typ == 'doubleValue':
        return float(value)
    elif typ == 'blobValue':
        return value