import boto3
ssm_client = boto3.client('ssm')

# S3 Buckets
SOFT_ERROR_BUCKET = 'rl-dev-rfid-inventory-error'
HARD_ERROR_BUCKET = 'rl-dev-rfid-inventory-error'

# S3 PREFIX
SOFT_ERROR_PREFIX = 'store-inventory-redshift/softError/'
HARD_ERROR_PREFIX = 'store-inventory-redshift/hardError/'
LU_ERROR_PREFIX = f'{SOFT_ERROR_PREFIX}LU/'

# REDSHIFT PARAMETER REFERENCE
PARAMETER_PATH = '/RL/DNA/RFID/DEV/'
parameters = ssm_client.get_parameters_by_path(
    Path=PARAMETER_PATH,
    Recursive=True,
    WithDecryption=True
)['Parameters']
redshift_config_params = {}
for param in parameters : redshift_config_params[param['Name']] = param['Value']
print(redshift_config_params)

# # REDSHIFT DEFAULT
REDSHIFT_CLUSTER = redshift_config_params[f'{PARAMETER_PATH}REDSHIFT/CLUSTER']
REDSHIFT_DATABASE = redshift_config_params[f'{PARAMETER_PATH}REDSHIFT/DATABASE']
REDSHIFT_USER = redshift_config_params[f'{PARAMETER_PATH}REDSHIFT/USER']
REDSHIFT_IAM_ROLE = redshift_config_params[f'{PARAMETER_PATH}REDSHIFT/IAM']

# # REDSHIFT CUSTOMER DB CONFIG
# REDSHIFT_CUSTOMER_DB = redshift_config_params[f'{PARAMETER_PATH}CUSTOMER/DATABASE']
# REDSHIFT_CUSTOMER_DB_USER = redshift_config_params[f'{PARAMETER_PATH}CUSTOMER/USER']

# REDSHIFT_CLUSTER = 'rl-qa-redshift'
# REDSHIFT_DATABASE = 'sample'
# REDSHIFT_USER = 'pshresth'
# REDSHIFT_IAM_ROLE = 'arn:aws:iam::320251637255:role/rl-mySpectrumRole'

# REDSHIFT CONN RETRY CONFIG
REDSHIFT_MAX_CONN_RETRIES = 3
REDSHIFT_CONN_WAIT_SECONDS = 10

# REDSHIFT SCHEMA
REDSHIFT_STAGE_SCHEMA = 'global_dev_stg'
REDSHIFT_STAGE_PROTO_SCHEMA = 'global_dev_stg'
REDSHIFT_TARGET_SCHEMA = 'global_dev_data'
REDSHIFT_CUSTOMER_TARGET_SCHEMA = 'global_dev_data'
REDSHIFT_DATA_SCHEMA = 'global_dev_data'
REDSHIFT_VIEW_SCHEMA = 'global_dev_data'

# LAMBDA RELAYS
RELAY_LAMBDA = None

# SNS Topics and Details
SNS_ERROR_TOPIC_ARN = 'arn:aws:sns:us-east-1:320251637255:rl-dev-rfid-error-notification'
SNS_WARN_TOPIC_ARN = 'arn:aws:sns:us-east-1:320251637255:rl-dev-rfid-warn-notification'
SNS_SUPPORT_TOPIC_ARN = 'arn:aws:sns:us-east-1:320251637255:rl-dev-rfid-support-notification'
SNS_ERROR_NOTIFICATION_SUBJECT = 'ERROR: Exception has occurred during RFID Traffic ETL.'
SNS_ERROR_NOTIFICATION_MESSAGE = 'This email is to notify that an error has been encountered while processing the ' \
                                 'request. '
SNS_FAILURE_NOTIFICATION_SUBJECT = 'ERROR: Exception has occurred during RFID Traffic ETL.'
SNS_FAILURE_NOTIFICATION_MESSAGE = "This email contains the details of failure: "
SNS_EMPTY_NOTIFICATION_SUBJECT = 'ERROR: Source Does not contain Data'
SNS_EMPTY_NOTIFICATION_MESSAGE = "This email contains the details of failure: "
SNS_SERVICE_DOWN_NOTIFICATION_SUBJECT = 'WARN: Service is Down'
SNS_SERVICE_DOWN_NOTIFICATION_MESSAGE = 'This email contains the details of warning in the service.'
SNS_INSUFFICIENT_PRIVILEGES_NOTIFICATION_SUBJECT = 'WARN: Insufficient Privileges'
SNS_INSUFFICIENT_PRIVILEGES_NOTIFICATION_MESSAGE = 'This email contains the details of the warning in the access ' \
                                                   'privileges. '
SNS_DESTINATION_KEY = '\nFilename: '
SNS_DESTINATION_BUCKET = '\nSource: '
SNS_ERROR_DETAILS = '\nError: '
SNS_WARNING_DETAILS = '\nWarning: '
SNS_REALTIME_PROCESSING = True
SNS_SERVICE_NAME = '\nService Name: '
SNS_PRIVILEGE_NAME = '\nPrivilege Name: '

V_CHAIN_PFS = 'PFS'
V_CHAIN_RLS = 'RLS'