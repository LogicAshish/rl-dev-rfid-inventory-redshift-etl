CREATE TABLE global_dev_stg.tmp_proto_sq_inventory(
"SiteName" VARCHAR(2048),
"LocationName" VARCHAR(2048),
"UPC" VARCHAR(2048),
"CurrentInventory" VARCHAR(2048),
"size" VARCHAR(2048),
"department" VARCHAR(2048),
"sku" VARCHAR(2048),
"season" VARCHAR(2048),
"color" VARCHAR(2048),
"ProdName" VARCHAR(2048),
"LIN" VARCHAR(2048),
"VendorStyle" VARCHAR(2048),
"Division" VARCHAR(2048),
"date_updated"VARCHAR(2048)
);