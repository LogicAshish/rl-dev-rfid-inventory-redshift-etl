CREATE TABLE IF NOT EXISTS global_dev_stg.tmp_proto_exp_store_validation
(
	i_site_name VARCHAR(2048)   
	,site_name VARCHAR(2048)   
	,facility VARCHAR(2048)   
	,facility_id INTEGER   
	,o_facility_id INTEGER   
	,length INTEGER   
	,facility_name VARCHAR(2048)   
	,division VARCHAR(2048)   
	,o_division VARCHAR(2048)   
	,season VARCHAR(2048)   
	,o_season VARCHAR(2048)   
	,department VARCHAR(2048)   
	,o_department VARCHAR(2048)   
	,vendor_style VARCHAR(2048)   
	,o_vendor_style VARCHAR(2048)   
	,product_name VARCHAR(2048)   
	,color VARCHAR(2048)   
	,o_color VARCHAR(2048)   
	,size VARCHAR(2048)   
	,o_size VARCHAR(2048)   
	,upc VARCHAR(2048)   
	,scan_id VARCHAR(2068)   
	,"location" VARCHAR(2048)   
	,current_inventory VARCHAR(2048)   
	,o_current_inventory INTEGER   
	,long_item_id VARCHAR(2048)   
	,chain_id INTEGER   
)
