CREATE TABLE IF NOT EXISTS global_dev_stg.tmp_proto_missing_inventory_merge
(
	chain_id INTEGER   
	,global_facility_key INTEGER   
	,facility_name VARCHAR(2048)   
	,scan_id VARCHAR(2068)   
	,regional_product_key BIGINT   
	,longitem_id VARCHAR(2048)   
	,product_name VARCHAR(2048)   
	,error_desc VARCHAR(28)   
	,"location" VARCHAR(2048)   
	,inventory_qty INTEGER   
)
