CREATE TABLE IF NOT EXISTS global_dev_stg.tmp_proto_rfid_exception
(
	exception_id VARCHAR   
	,exception_desc VARCHAR
	,chain_id VARCHAR   
	,scan_id VARCHAR(2068)   
	,regional_product_key VARCHAR   
	,rtl_dept_id VARCHAR   
	,rtl_class_id VARCHAR   
	,rtl_color_id VARCHAR(2048)   
	,rtl_size_id VARCHAR   
	,rtl_facility_cd VARCHAR   
	,store_group VARCHAR   
	,classvendorstyle_cd VARCHAR(2048)   
	,create_ts TIMESTAMP    
	,update_ts TIMESTAMP   
)

-- CREATE TABLE IF NOT EXISTS global_dev_stg.tmp_proto_rfid_exception
-- (
-- 	exception_id INTEGER   
-- 	,exception_desc VARCHAR(41)   
-- 	,chain_id INTEGER   
-- 	,scan_id VARCHAR(2068)   
-- 	,regional_product_key BIGINT   
-- 	,rtl_dept_id INTEGER   
-- 	,rtl_class_id INTEGER   
-- 	,rtl_color_id VARCHAR(2048)   
-- 	,rtl_size_id INTEGER   
-- 	,rtl_facility_cd INTEGER   
-- 	,store_group VARCHAR(11)   
-- 	,classvendorstyle_cd VARCHAR(2048)   
-- 	,create_ts TIMESTAMP    
-- 	,update_ts TIMESTAMP   
-- )
