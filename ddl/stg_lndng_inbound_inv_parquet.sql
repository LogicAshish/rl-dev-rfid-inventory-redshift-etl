CREATE TABLE global_dev_stg.stg_lndng_inbound_inv_parquet(
"site_name" VARCHAR(2048),
"zone_name" VARCHAR(2048),
"product_code" VARCHAR(2048),
"prodcount" VARCHAR(2048),
"size" VARCHAR(2048),
"department" VARCHAR(2048),
"sku" VARCHAR(2048),
"season" VARCHAR(2048),
"color" VARCHAR(2048),
"Product_name" VARCHAR(2048),
"LIN" VARCHAR(2048),
"vendor_style" VARCHAR(2048),
"Division" VARCHAR(2048),
"date_updated"VARCHAR(2048)
);