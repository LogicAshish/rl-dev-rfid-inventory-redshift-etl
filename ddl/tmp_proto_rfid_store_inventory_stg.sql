CREATE TABLE IF NOT EXISTS global_dev_stg.tmp_proto_rfid_store_inventory_stg
(
	chain_id INTEGER   
	,rtl_facility_cd INTEGER   
	,facility_name VARCHAR(2048)   
	,division VARCHAR(2048)   
	,season VARCHAR(2048)   
	,department VARCHAR(2048)   
	,vendorstyle VARCHAR(2048)   
	,product_name VARCHAR(2048)   
	,color VARCHAR(2048)   
	,size VARCHAR(2048)   
	,scan_id VARCHAR(2068)   
	,regional_product_key BIGINT   
	,"location" VARCHAR(2048)   
	,inventory_qty INTEGER   
	,longitem_id VARCHAR(2048)   
)
