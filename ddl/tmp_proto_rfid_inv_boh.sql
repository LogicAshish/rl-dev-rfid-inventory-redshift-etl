CREATE TABLE IF NOT EXISTS global_dev_stg.tmp_proto_rfid_inv_boh
(
	chain_id INTEGER   
	,global_facility_key BIGINT   
	,load_date DATE   
	,regional_product_key BIGINT   
	,"location" VARCHAR(2048)   
	,inventory_qty BIGINT   
	,rnk BIGINT   
)
