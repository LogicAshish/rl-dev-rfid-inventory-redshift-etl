CREATE TABLE IF NOT EXISTS global_dev_stg.tmp_proto_scan_code_modified
(
	regional_product_key BIGINT
	,scan_id VARCHAR(20)
	,chain_id BIGINT
)