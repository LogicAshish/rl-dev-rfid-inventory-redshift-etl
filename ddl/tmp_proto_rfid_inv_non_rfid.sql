CREATE TABLE IF NOT EXISTS global_dev_stg.tmp_proto_rfid_inv_non_rfid
(
	global_facility_key INTEGER   
	,periodend_dt DATE   
	,regional_product_key BIGINT   
	,sales_qty BIGINT   
	,salescost_amt NUMERIC(38,6)   
	,salesretail_amt NUMERIC(38,4)   
)
