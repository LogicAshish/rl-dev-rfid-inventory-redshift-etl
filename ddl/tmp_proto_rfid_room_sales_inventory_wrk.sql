CREATE TABLE IF NOT EXISTS global_dev_stg.tmp_proto_rfid_room_sales_inventory_wrk
(
	chain_id INTEGER   
	,global_facility_key BIGINT   
	,load_date DATE   
	,regional_product_key BIGINT   
	,"location" VARCHAR(2048)   
	,inventory_qty BIGINT   
	,sales_qty BIGINT   
	,salescost_amt NUMERIC(38,6)   
	,salesretail_amt NUMERIC(38,4)   
)
