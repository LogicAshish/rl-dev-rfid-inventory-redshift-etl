CREATE TABLE IF NOT EXISTS global_dev_stg.tmp_proto_rfid_store_inventory
(
	chain_id INTEGER   
	,global_facility_key BIGINT   
	,facility_name VARCHAR(2048)   
	,scan_id VARCHAR(2068)   
	,regional_product_key BIGINT   
	,longitem_id VARCHAR(2048)   
	,product_name VARCHAR(2048)   
	,"location" VARCHAR(2048)   
	,location_category VARCHAR(50)   
	,inventory_qty INTEGER   
	,calc_inv_qty INTEGER   
	,load_date DATE   
	,cc_ind VARCHAR(256)   
	,create_ts TIMESTAMP 
	,update_ts TIMESTAMP
	,source_id INTEGER   
)
