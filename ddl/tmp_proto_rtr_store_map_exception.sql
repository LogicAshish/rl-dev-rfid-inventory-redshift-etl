CREATE TABLE IF NOT EXISTS global_dev_stg.tmp_proto_rtr_store_map_exception
(
	site_name VARCHAR(2048)   
	,chain_id INTEGER   
	,rtl_facility_cd INTEGER   
	,facility_name VARCHAR(2048)   
	,o_division VARCHAR(2048)   
	,o_season VARCHAR(2048)   
	,o_department VARCHAR(2048)   
	,o_vendor_style VARCHAR(2048)   
	,product_name VARCHAR(2048)   
	,o_color VARCHAR(2048)   
	,o_size VARCHAR(2048)   
	,scan_id VARCHAR(2068)   
	,"location" VARCHAR(2048)   
	,current_inventory INTEGER   
	,long_item_id VARCHAR(2048)   
	,scan_id_lkp VARCHAR(20)   
	,chain_id_lkp BIGINT   
	,regional_product_key BIGINT   
)
