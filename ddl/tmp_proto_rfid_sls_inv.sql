CREATE TABLE IF NOT EXISTS global_dev_stg.tmp_proto_rfid_sls_inv
(
	chain_id INTEGER   
	,global_facility_key BIGINT   
	,regional_product_key BIGINT   
	,load_date DATE   
	,"location" VARCHAR(20)   
	,inventory_qty BIGINT   
	,sales_qty BIGINT   
	,salescost_amt NUMERIC(38,6)   
	,salesretail_amt NUMERIC(38,4)   
	,gm NUMERIC(38,6)   
)
