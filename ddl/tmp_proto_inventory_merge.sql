CREATE TABLE IF NOT EXISTS global_dev_stg.tmp_proto_inventory_merge
(
	flag VARCHAR(10)   
	,chain_id INTEGER   
	,rtl_facility_cd INTEGER   
	,facility_name VARCHAR(2048)   
	,scan_id VARCHAR(2068)   
	,regional_product_key BIGINT   
	,longitem_id VARCHAR(2048)   
	,product_name VARCHAR(2048)   
	,"location" VARCHAR(2048)   
	,location_category VARCHAR(50)   
	,inventory_qty INTEGER   
)

-- CREATE TABLE IF NOT EXISTS global_dev_stg.tmp_proto_inventory_merge
-- (
-- 	flag VARCHAR(10)   
-- 	,chain_id INTEGER   
-- 	,rtl_facility_cd INTEGER   
-- 	,facility_name VARCHAR(2048)   
-- 	,scan_id VARCHAR(2068)   
-- 	,regional_product_key BIGINT   
-- 	,longitem_id VARCHAR(2048)   
-- 	,product_name VARCHAR(2048)   
-- 	,"location" VARCHAR(2048)   
-- 	,location_category VARCHAR(50)   
-- 	,inventory_qty INTEGER   
-- 	,calc_inv_qty INTEGER   
-- 	,cc_ind VARCHAR(256)   
-- )
