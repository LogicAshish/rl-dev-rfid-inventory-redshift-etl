CREATE TABLE IF NOT EXISTS global_dev_stg.tmp_proto_rfid_location_xref
(
	chain_id BIGINT   
	,global_facility_key BIGINT   
	,class_vendor_style VARCHAR(2048)   
	,"location" VARCHAR(2048)   
)
