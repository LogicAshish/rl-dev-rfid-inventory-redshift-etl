CREATE TABLE IF NOT EXISTS global_dev_stg.tmp_proto_rfid_room_inv_temp
(
	chain_id BIGINT   
	,global_facility_key BIGINT   
	,load_date DATE   
	,regional_product_key BIGINT   
	,class_vendor_style VARCHAR(2048)   
	,"location" VARCHAR(2048)   
	,inventory_qty INTEGER   
	,rnk BIGINT   
)
