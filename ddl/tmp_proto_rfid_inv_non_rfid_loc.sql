CREATE TABLE IF NOT EXISTS global_dev_stg.tmp_proto_rfid_inv_non_rfid_loc
(
	chain_id INTEGER   
	,global_facility_key INTEGER   
	,periodend_dt DATE   
	,regional_product_key BIGINT   
	,sales_qty BIGINT   
	,inventory_qty INTEGER   
	,class_vendor_style VARCHAR(2048)   
	,"location" VARCHAR(2048)   
)
