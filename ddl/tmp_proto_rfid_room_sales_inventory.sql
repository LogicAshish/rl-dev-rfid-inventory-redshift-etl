CREATE TABLE IF NOT EXISTS global_dev_stg.tmp_proto_rfid_room_sales_inventory
(
	chain_id INTEGER   
	,global_facility_key BIGINT   
	,regional_product_key BIGINT   
	,load_date DATE   
	,"location" VARCHAR(2048)   
	,inventory_qty INTEGER   
	,sales_qty INTEGER   
	,salescost_amt NUMERIC(18,4)   
	,salesretail_amt NUMERIC(18,4)   
	,gm NUMERIC(18,4)   
	,tgt_cpty INTEGER   
	,sqft INTEGER   
	,currency_id INTEGER   
	,create_ts TIMESTAMP 
)