import constants

stage_schema = constants.REDSHIFT_STAGE_SCHEMA
temp_schema = constants.REDSHIFT_STAGE_SCHEMA
stage_proto_schema = constants.REDSHIFT_STAGE_PROTO_SCHEMA
target_schema = constants.REDSHIFT_TARGET_SCHEMA
customer_target_schema = constants.REDSHIFT_CUSTOMER_TARGET_SCHEMA
data_schema = constants.REDSHIFT_DATA_SCHEMA
view_schema = constants.REDSHIFT_VIEW_SCHEMA

table = {
    'stage_landing': {
        'f_store_inventory_parquet': f'{stage_schema}.stg_lndng_inbound_inv_parquet',
    },
    'stg_proto': {
        'f_store_inventory_parquet': f'{stage_schema}.stg_proto_inbound_inv_parquet',
    },
    'tmp_proto': {
        'tmp_sq_inventory': f'{temp_schema}.TMP_PROTO_SQ_INVENTORY',
        'tmp_scan_code_modified': f'{temp_schema}.TMP_PROTO_SCAN_CODE_MODIFIED',
        'tmp_exp_store_validation': f'{temp_schema}.TMP_PROTO_EXP_STORE_VALIDATION',
        'tmp_rtr_store_map_exception': f'{temp_schema}.TMP_PROTO_RTR_STORE_MAP_EXCEPTION',
        'tmp_rfid_store_inventory_stg': f'{temp_schema}.TMP_PROTO_RFID_STORE_INVENTORY_STG',
        'tmp_rfid_exception': f'{temp_schema}.TMP_PROTO_RFID_EXCEPTION',
        'tmp_rfid_missing_inventory_stg': f'{temp_schema}.TMP_PROTO_RFID_MISSING_INVENTORY_STG',
        'tmp_inventory_merge': f'{temp_schema}.TMP_PROTO_INVENTORY_MERGE',
        'tmp_missing_inventory_merge': f'{temp_schema}.TMP_PROTO_MISSING_INVENTORY_MERGE',
        'tmp_rfid_location_xref': f'{temp_schema}.TMP_PROTO_RFID_LOCATION_XREF',
        'tmp_rfid_room_inv_temp': f'{temp_schema}.TMP_PROTO_RFID_ROOM_INV_TEMP',
        'tmp_rfid_inv_non_rfid': f'{temp_schema}.TMP_PROTO_RFID_INV_NON_RFID',
        'tmp_rfid_inv_non_rfid_loc': f'{temp_schema}.TMP_PROTO_RFID_INV_NON_RFID_LOC',
        'tmp_rfid_inv_boh': f'{temp_schema}.TMP_PROTO_RFID_INV_BOH',
        'tmp_rfid_sls_inv': f'{temp_schema}.TMP_PROTO_RFID_SLS_INV',
        'tmp_rfid_room_sales_inventory_wrk': f'{temp_schema}.TMP_PROTO_RFID_ROOM_SALES_INVENTORY_WRK',
        'tmp_rfid_room_sales_inventory': f'{temp_schema}.TMP_PROTO_RFID_ROOM_SALES_INVENTORY',
    },
    'stg_prefix': {
        'f_store_inventory_parquet': f'{stage_schema}.stg_f_inbound_inv_parquet',
    },
    'tgt': {
        'rfid_store_inventory': f'{target_schema}.RFID_STORE_INVENTORY',
        'rfid_missing_inventory': f'{target_schema}.RFID_MISSING_INVENTORY',
        'rfid_room_sales_inventory': f'{target_schema}.RFID_ROOM_SALES_INVENTORY',
    },
    'tmp': {
        'tmp_sq_inventory': f'{temp_schema}.TMP_SQ_INVENTORY',
        'tmp_scan_code_modified': f'{temp_schema}.TMP_SCAN_CODE_MODIFIED',
        'tmp_exp_store_validation': f'{temp_schema}.TMP_EXP_STORE_VALIDATION',
        'tmp_rtr_store_map_exception': f'{temp_schema}.TMP_RTR_STORE_MAP_EXCEPTION',
        'tmp_rfid_store_inventory_stg': f'{temp_schema}.TMP_RFID_STORE_INVENTORY_STG',
        'tmp_rfid_exception': f'{temp_schema}.TMP_RFID_EXCEPTION',
        'tmp_rfid_missing_inventory_stg': f'{temp_schema}.TMP_RFID_MISSING_INVENTORY_STG',
        'tmp_inventory_merge': f'{temp_schema}.TMP_INVENTORY_MERGE',
        'tmp_missing_inventory_merge': f'{temp_schema}.TMP_MISSING_INVENTORY_MERGE',
        'tmp_rfid_location_xref_pfs': f'{temp_schema}.TMP_RFID_LOCATION_XREF_PFS',
        'tmp_rfid_room_inv_temp_pfs': f'{temp_schema}.TMP_RFID_ROOM_INV_TEMP_PFS',
        'tmp_rfid_inv_non_rfid_pfs': f'{temp_schema}.TMP_RFID_INV_NON_RFID_PFS',
        'tmp_rfid_inv_non_rfid_loc_pfs': f'{temp_schema}.TMP_RFID_INV_NON_RFID_LOC_PFS',
        'tmp_rfid_inv_boh_pfs': f'{temp_schema}.TMP_RFID_INV_BOH_PFS',
        'tmp_rfid_sls_inv_pfs': f'{temp_schema}.TMP_RFID_SLS_INV_PFS',
        'tmp_rfid_room_sales_inventory_wrk_pfs': f'{temp_schema}.TMP_RFID_ROOM_SALES_INVENTORY_WRK_PFS',
        'tmp_rfid_room_sales_inventory_pfs': f'{temp_schema}.TMP_RFID_ROOM_SALES_INVENTORY_PFS',
        'tmp_rfid_location_xref_rls': f'{temp_schema}.TMP_RFID_LOCATION_XREF_RLS',
        'tmp_rfid_room_inv_temp_rls': f'{temp_schema}.TMP_RFID_ROOM_INV_TEMP_RLS',
        'tmp_rfid_inv_non_rfid_rls': f'{temp_schema}.TMP_RFID_INV_NON_RFID_RLS',
        'tmp_rfid_inv_non_rfid_loc_rls': f'{temp_schema}.TMP_RFID_INV_NON_RFID_LOC_RLS',
        'tmp_rfid_inv_boh_rls': f'{temp_schema}.TMP_RFID_INV_BOH_RLS',
        'tmp_rfid_sls_inv_rls': f'{temp_schema}.TMP_RFID_SLS_INV_RLS',
        'tmp_rfid_room_sales_inventory_wrk_rls': f'{temp_schema}.TMP_RFID_ROOM_SALES_INVENTORY_WRK_RLS',
        'tmp_rfid_room_sales_inventory_rls': f'{temp_schema}.TMP_RFID_ROOM_SALES_INVENTORY_RLS',
    },
    'lu': {
        'd_global_facility': f'{data_schema}.GLOBAL_FACILITY_OUTER',
        'd_edw_source': f'{data_schema}.REGIONAL_PRODUCT',
        'd_regional_product': f'{data_schema}.REGIONAL_PRODUCT',
        'd_scan_code': f'{data_schema}.SCAN_CODE',
        'd_rfid_location_xref': f'{data_schema}.RFID_LOCATION_XREF',
        'd_rfid_store_tgt_cpty': f'{data_schema}.RFID_STORE_TGT_CPTY',
        'd_regional_product_outer': f'{data_schema}.REGIONAL_PRODUCT_OUTER',
        'd_rls_display_item_xref': f'{data_schema}.RLS_DISPLAY_ITEM_XREF',
        'd_facility_currency': f'{data_schema}.FACILITY_CURRENCY',
        'd_fact_sellout_daily': f'{data_schema}.FACT_SELLOUT_DAILY',
        'd_rfid_store_inventory': f'{data_schema}.RFID_STORE_INVENTORY',
    },
    'view': {
    }
}
