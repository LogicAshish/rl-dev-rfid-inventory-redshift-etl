import time
import boto3
import logging
import re

from utils import redshift
from utils import notifications

from etl import sqls

import constants

# CLIENTS
s3_client = boto3.client('s3')

# LOGGING
logger = logging.getLogger('etl')
logger.setLevel(logging.DEBUG)


def etl_bulk_load(bucket, prefix, batch) :
    """
        ETL entrypoint to process all files in  S3 prefix
    """
    pass
    # try :
    #     s3_prefix = f's3://{bucket}/{prefix}'
    #     logger.info(f"Initiating BULK ETL for : {s3_prefix}")
    #     logger.info(f"batch: {batch}")

    #     # batch can only have alphanumeric and _
    #     regex = '([^A-Za-z0-9_]+)'
    #     if re.search(regex, batch):
    #         logger.error(f"Invalid batch : {batch}. Ensure batch is alphanumeric")
    #         raise

    #     # timestamp
    #     ms_timestamp = int(time.time() * 1000)
    # except Exception as e:
    #     logger.error(f"Pre ETL load operation failed.")
    #     return

    # logger.debug(f"STAGE LOAD INITIATE")
    # try:
    #     # STAGE LOAD ===========================================================

    #     # load all pre-stage tables ============================================
    #     process = "PRE STAGE TABLE LOAD"
    #     lu_error_bucket = constants.SOFT_ERROR_BUCKET
    #     lu_error_prefix = f'{constants.LU_ERROR_PREFIX}{batch}/{ms_timestamp}/'
    #     SQL = sqls.transact_pre_stage_load_sql(batch, s3_prefix, lu_error_bucket, lu_error_prefix)
    #     q = redshift.execute_sql(SQL, name=process)
    #     redshift.wait_for_queries([q])

    #     # load all stage tables ================================================
    #     process = "STAGE TABLE LOAD : all"
    #     SQL = sqls.transact_stage_load_sql(batch)
    #     q = redshift.execute_sql(SQL, name=process)
    #     redshift.wait_for_queries([q])

    #     # TARGET LOAD ==========================================================

    #     # f_sales_txn_header ===================================================
    #     process = "TARGET LOAD : f_sales_txn_header"
    #     SQL = sqls.transact_sql(
    #         sqls.target_delete_f_sales_txn_header_sql(batch),
    #         sqls.target_insert_f_sales_txn_header_sql(batch)
    #     )
    #     q = redshift.execute_sql(SQL, name=process)
    #     redshift.wait_for_queries([q])

    #     # f_sales_txn_item ===================================================
    #     process = "TARGET LOAD : f_sales_txn_item"
    #     SQL = sqls.transact_sql(
    #         sqls.target_delete_f_sales_txn_item_sql(batch),
    #         sqls.target_insert_f_sales_txn_item_sql(batch)
    #     )
    #     q = redshift.execute_sql(SQL, name=process)
    #     redshift.wait_for_queries([q])

    #     # f_sales_txn_item_tax ================================================
    #     process = "TARGET LOAD : f_sales_txn_item_tax"
    #     SQL = sqls.transact_sql(
    #         sqls.target_delete_f_sales_txn_item_tax_sql(batch),
    #         sqls.target_insert_f_sales_txn_item_tax_sql(batch)
    #     )
    #     q = redshift.execute_sql(SQL, name=process)
    #     redshift.wait_for_queries([q])

    #     # f_sales_txn_item_discount ===========================================
    #     process = "TARGET LOAD : f_sales_txn_item_discount"
    #     SQL = sqls.transact_sql(
    #         sqls.target_delete_f_sales_txn_item_discount_sql(batch),
    #         sqls.target_insert_f_sales_txn_item_discount_sql(batch)
    #     )
    #     q = redshift.execute_sql(SQL, name=process)
    #     redshift.wait_for_queries([q])

    #     # f_sales_txn_tender ==================================================
    #     process = "TARGET LOAD : f_sales_txn_tender"
    #     SQL = sqls.transact_sql(
    #         sqls.target_delete_f_sales_txn_tender_sql(batch),
    #         sqls.target_insert_f_sales_txn_tender_sql(batch)
    #     )
    #     q = redshift.execute_sql(SQL, name=process)
    #     redshift.wait_for_queries([q])

    #     # f_sales_txn_customer =================================================
    #     # user = 'rl_dna_restricted_eu_dev_etl_usr'
    #     process = "TARGET LOAD : f_sales_txn_customer"
    #     user = constants.REDSHIFT_CUSTOMER_DB_USER
    #     db = constants.REDSHIFT_CUSTOMER_DB
    #     SQL = sqls.transact_sql(
    #         sqls.target_delete_f_sales_txn_customer_sql(batch),
    #         sqls.target_insert_f_sales_txn_customer_sql(batch)
    #     )
    #     q = redshift.execute_sql(SQL, name=process, database=db, user=user)
    #     redshift.wait_for_queries([q])

    #     logger.info("ETL OPERATION SUCCESS")

    # except Exception as e:
    #     logger.error(f"ETL OPERATION FAILED AT : {process}")
    #     logger.error(f"ERROR : {e.__class__.__name__} : {str(e)}")

    #     # no hard error movement for bulk loads
    #     # only notifications
    #     if process in ["PRE STAGE TABLE LOAD"]:
    #         error = "File Invalid. Unable to Initiate COPY for a parquet file in given prefix."
    #         # notifications.initiate_error_notification(bucket, prefix, error)
    #     else :
    #         error = f"ETL Operation Raised Error : {e.__class__.__name__}:{str(e)}"
    #         # notifications.initiate_error_notification(bucket, prefix, error)
    #         pass
    # finally:
    #     # clean all stage tables ===============================================
    #     process = "STAGE TABLE CLEAN : ALL"
    #     logger.debug(f"START : {process}")
    #     SQL = sqls.transact_stage_clean_sql(batch)
    #     q = redshift.execute_sql(SQL, process)
    #     redshift.wait_for_queries([q])
    #     logger.debug(f"END : {process}")