from etl import schemas
from datetime import datetime
import constants

# REDSHIFT IAM ROLE
iam_role = constants.REDSHIFT_IAM_ROLE


def transact_landing_load_sql(parquet_file):
    """
        COPY parquet data to landing table
        parquet file : s3 path containing parquet file . eg. s3://<bucket>/<key>
    """
    splitted = parquet_file.split('/')

    stg_landing_table = ""

    if splitted[3] == 'bigquery-glue-inbound-test':
        stg_landing_table = f"{schemas.table['stage_landing']['f_store_inventory_parquet']}"

    print("Staging table:" + stg_landing_table)

    SQL = f"""
        BEGIN; 
        {stage_landing_table_lock_sql(splitted[3])}

        -- LOAD PARQUET TO TABLE
        COPY {stg_landing_table}
        FROM '{parquet_file}'
        IAM_ROLE '{iam_role}'
        CSV
        ;

        COMMIT;
        END;
        """
    return SQL


def stage_landing_table_lock_sql(response_type):

    if response_type == 'bigquery-glue-inbound-test' or response_type == 'Inventory':
        stg_landing_table = f"{schemas.table['stage_landing']['f_store_inventory_parquet']}"
        SQL = f"""
            LOCK {stg_landing_table};
        """
    return SQL


def transact_pre_stage_load_landing_sql(batch, lu_error_bucket, lu_error_prefix, response_type):
    return transact_sql(
        stage_landing_table_lock_sql(response_type),
        pre_stage_create_tables_sql(batch, response_type),
        pre_stage_load_inv_parquet_from_landing_sql(batch, response_type),
        pre_stage_clear_landing_parquet(batch, response_type),
        pre_temp_sq_inv_create_tables_sql(batch, response_type),
        pre_temp_scan_code_modified_create_tables_sql(batch, response_type),
        pre_temp_exp_store_validation_create_tables_sql(batch, response_type),
        pre_temp_rtr_store_map_exception_create_tables_sql(
            batch, response_type),
        pre_temp_rfid_store_inventory_stg_create_tables_sql(
            batch, response_type),
        pre_temp_rfid_exception_create_tables_sql(batch, response_type),
        pre_temp_rfid_missing_inventory_stg_create_tables_sql(
            batch, response_type),
        pre_temp_inventory_merge_create_tables_sql(batch, response_type),
        pre_temp_missing_inventory_merge_create_tables_sql(
            batch, response_type),
        pre_temp_rfid_location_xref_create_tables_sql(
            batch, response_type=constants.V_CHAIN_PFS),
        pre_temp_rfid_room_inv_temp_create_tables_sql(
            batch, response_type=constants.V_CHAIN_PFS),
        pre_temp_rfid_inv_non_rfid_create_tables_sql(
            batch, response_type=constants.V_CHAIN_PFS),
        pre_temp_rfid_inv_non_rfid_loc_create_tables_sql(
            batch, response_type=constants.V_CHAIN_PFS),
        pre_temp_rfid_inv_boh_create_tables_sql(
            batch, response_type=constants.V_CHAIN_PFS),
        pre_temp_rfid_sls_inv_create_tables_sql(
            batch, response_type=constants.V_CHAIN_PFS),
        pre_temp_rfid_room_sales_inventory_wrk_create_tables_sql(
            batch, response_type=constants.V_CHAIN_PFS),
        pre_temp_rfid_room_sales_inventory_create_tables_sql(
            batch, response_type=constants.V_CHAIN_PFS),
        pre_temp_rfid_location_xref_create_tables_sql(
            batch, response_type=constants.V_CHAIN_RLS),
        pre_temp_rfid_room_inv_temp_create_tables_sql(
            batch, response_type=constants.V_CHAIN_RLS),
        pre_temp_rfid_inv_non_rfid_create_tables_sql(
            batch, response_type=constants.V_CHAIN_RLS),
        pre_temp_rfid_inv_non_rfid_loc_create_tables_sql(
            batch, response_type=constants.V_CHAIN_RLS),
        pre_temp_rfid_inv_boh_create_tables_sql(
            batch, response_type=constants.V_CHAIN_RLS),
        pre_temp_rfid_sls_inv_create_tables_sql(
            batch, response_type=constants.V_CHAIN_RLS),
        pre_temp_rfid_room_sales_inventory_wrk_create_tables_sql(
            batch, response_type=constants.V_CHAIN_RLS),
        pre_temp_rfid_room_sales_inventory_create_tables_sql(
            batch, response_type=constants.V_CHAIN_RLS),
    )


def landing_count_sql(response_type):

    if response_type == 'Inventory':
        stg_landing_f_Inventory_parquet = f"{schemas.table['stage_landing']['f_store_inventory_parquet']}"
        SQL = f"""
            SELECT count(*) FROM {stg_landing_f_Inventory_parquet};
        """
    return SQL


def pre_stage_create_tables_sql(batch, response_type):
    if response_type == 'Inventory':
        # proto & stage table
        proto_stg_f_store_inventory_parquet = schemas.table['stg_proto']['f_store_inventory_parquet']
        temp_stg_f_store_inventory_parquet = f'{schemas.table["stg_prefix"]["f_store_inventory_parquet"]}_{batch}'

        SQL = f"""
        -- CREATE SALES DATA PARQUET TABLE
        CREATE TABLE IF NOT EXISTS {temp_stg_f_store_inventory_parquet} (LIKE {proto_stg_f_store_inventory_parquet});
        """
    return SQL


def pre_temp_sq_inv_create_tables_sql(batch, response_type):
    proto_temp_sq_inventory = schemas.table['tmp_proto']['tmp_sq_inventory']
    temp_temp_sq_inventory = f'{schemas.table["tmp"]["tmp_sq_inventory"]}_{batch}'

    SQL = f"""
    -- CREATE SALES DATA PARQUET TABLE
    CREATE TABLE IF NOT EXISTS {temp_temp_sq_inventory} (LIKE {proto_temp_sq_inventory});
    """
    return SQL


def pre_temp_scan_code_modified_create_tables_sql(batch, response_type):
    proto_temp_scan_code_modified = schemas.table['tmp_proto']['tmp_scan_code_modified']
    temp_temp_scan_code_modified = f'{schemas.table["tmp"]["tmp_scan_code_modified"]}_{batch}'

    SQL = f"""
    -- CREATE SALES DATA PARQUET TABLE
    CREATE TABLE IF NOT EXISTS {temp_temp_scan_code_modified} (LIKE {proto_temp_scan_code_modified});
    """
    return SQL


def pre_temp_exp_store_validation_create_tables_sql(batch, response_type):
    proto_temp_exp_store_validation = schemas.table['tmp_proto']['tmp_exp_store_validation']
    temp_temp_exp_store_validation = f'{schemas.table["tmp"]["tmp_exp_store_validation"]}_{batch}'

    SQL = f"""
    -- CREATE SALES DATA PARQUET TABLE
    CREATE TABLE IF NOT EXISTS {temp_temp_exp_store_validation} (LIKE {proto_temp_exp_store_validation});
    """
    return SQL


def pre_temp_rtr_store_map_exception_create_tables_sql(batch, response_type):
    proto_temp_rtr_store_map_exception = schemas.table['tmp_proto']['tmp_rtr_store_map_exception']
    temp_temp_rtr_store_map_exception = f'{schemas.table["tmp"]["tmp_rtr_store_map_exception"]}_{batch}'

    SQL = f"""
    -- CREATE SALES DATA PARQUET TABLE
    CREATE TABLE IF NOT EXISTS {temp_temp_rtr_store_map_exception} (LIKE {proto_temp_rtr_store_map_exception});
    """
    return SQL


def pre_temp_rfid_store_inventory_stg_create_tables_sql(batch, response_type):
    proto_temp_rfid_store_inventory_stg = schemas.table['tmp_proto']['tmp_rfid_store_inventory_stg']
    temp_temp_rfid_store_inventory_stg = f'{schemas.table["tmp"]["tmp_rfid_store_inventory_stg"]}_{batch}'

    SQL = f"""
    -- CREATE SALES DATA PARQUET TABLE
    CREATE TABLE IF NOT EXISTS {temp_temp_rfid_store_inventory_stg} (LIKE {proto_temp_rfid_store_inventory_stg});
    """
    return SQL


def pre_temp_rfid_exception_create_tables_sql(batch, response_type):
    proto_temp_rfid_exception = schemas.table['tmp_proto']['tmp_rfid_exception']
    temp_temp_rfid_exception = f'{schemas.table["tmp"]["tmp_rfid_exception"]}_{batch}'

    SQL = f"""
    -- CREATE SALES DATA PARQUET TABLE
    CREATE TABLE IF NOT EXISTS {temp_temp_rfid_exception} (LIKE {proto_temp_rfid_exception});
    """
    return SQL


def pre_temp_rfid_missing_inventory_stg_create_tables_sql(batch, response_type):
    proto_temp_rfid_missing_inventory_stg = schemas.table[
        'tmp_proto']['tmp_rfid_missing_inventory_stg']
    temp_temp_rfid_missing_inventory_stg = f'{schemas.table["tmp"]["tmp_rfid_missing_inventory_stg"]}_{batch}'

    SQL = f"""
    -- CREATE SALES DATA PARQUET TABLE
    CREATE TABLE IF NOT EXISTS {temp_temp_rfid_missing_inventory_stg} (LIKE {proto_temp_rfid_missing_inventory_stg});
    """
    return SQL


def pre_temp_inventory_merge_create_tables_sql(batch, response_type):
    proto_temp_inventory_merge = schemas.table['tmp_proto']['tmp_inventory_merge']
    temp_temp_inventory_merge = f'{schemas.table["tmp"]["tmp_inventory_merge"]}_{batch}'

    SQL = f"""
    -- CREATE SALES DATA PARQUET TABLE
    CREATE TABLE IF NOT EXISTS {temp_temp_inventory_merge} (LIKE {proto_temp_inventory_merge});
    """
    return SQL


def pre_temp_missing_inventory_merge_create_tables_sql(batch, response_type):
    proto_temp_missing_inventory_merge = schemas.table['tmp_proto']['tmp_missing_inventory_merge']
    temp_temp_missing_inventory_merge = f'{schemas.table["tmp"]["tmp_missing_inventory_merge"]}_{batch}'

    SQL = f"""
    -- CREATE SALES DATA PARQUET TABLE
    CREATE TABLE IF NOT EXISTS {temp_temp_missing_inventory_merge} (LIKE {proto_temp_missing_inventory_merge});
    """
    return SQL


def pre_temp_rfid_location_xref_create_tables_sql(batch, response_type):
    proto_temp_rfid_location_xref = schemas.table['tmp_proto']['tmp_rfid_location_xref']
    if response_type == constants.V_CHAIN_PFS:
        temp_temp_rfid_location_xref = f'{schemas.table["tmp"]["tmp_rfid_location_xref_pfs"]}_{batch}'
    elif response_type == constants.V_CHAIN_RLS:
        temp_temp_rfid_location_xref = f'{schemas.table["tmp"]["tmp_rfid_location_xref_rls"]}_{batch}'
    SQL = f"""
    -- CREATE SALES DATA PARQUET TABLE
    CREATE TABLE IF NOT EXISTS {temp_temp_rfid_location_xref} (LIKE {proto_temp_rfid_location_xref});
    """
    return SQL


def pre_temp_rfid_room_inv_temp_create_tables_sql(batch, response_type):
    proto_temp_rfid_room_inv_temp = schemas.table['tmp_proto']['tmp_rfid_room_inv_temp']
    if response_type == constants.V_CHAIN_PFS:
        temp_temp_rfid_room_inv_temp = f'{schemas.table["tmp"]["tmp_rfid_room_inv_temp_pfs"]}_{batch}'
    elif response_type == constants.V_CHAIN_RLS:
        temp_temp_rfid_room_inv_temp = f'{schemas.table["tmp"]["tmp_rfid_room_inv_temp_rls"]}_{batch}'

    SQL = f"""
    -- CREATE SALES DATA PARQUET TABLE
    CREATE TABLE IF NOT EXISTS {temp_temp_rfid_room_inv_temp} (LIKE {proto_temp_rfid_room_inv_temp});
    """
    return SQL


def pre_temp_rfid_inv_non_rfid_create_tables_sql(batch, response_type):
    proto_temp_rfid_inv_non_rfid = schemas.table['tmp_proto']['tmp_rfid_inv_non_rfid']
    if response_type == constants.V_CHAIN_PFS:
        temp_temp_rfid_inv_non_rfid = f'{schemas.table["tmp"]["tmp_rfid_inv_non_rfid_pfs"]}_{batch}'
    elif response_type == constants.V_CHAIN_RLS:
        temp_temp_rfid_inv_non_rfid = f'{schemas.table["tmp"]["tmp_rfid_inv_non_rfid_rls"]}_{batch}'

    SQL = f"""
    -- CREATE SALES DATA PARQUET TABLE
    CREATE TABLE IF NOT EXISTS {temp_temp_rfid_inv_non_rfid} (LIKE {proto_temp_rfid_inv_non_rfid});
    """
    return SQL


def pre_temp_rfid_inv_non_rfid_loc_create_tables_sql(batch, response_type):
    proto_temp_rfid_inv_non_rfid_loc = schemas.table['tmp_proto']['tmp_rfid_inv_non_rfid_loc']
    if response_type == constants.V_CHAIN_PFS:
        temp_temp_rfid_inv_non_rfid_loc = f'{schemas.table["tmp"]["tmp_rfid_inv_non_rfid_loc_pfs"]}_{batch}'
    elif response_type == constants.V_CHAIN_RLS:
        temp_temp_rfid_inv_non_rfid_loc = f'{schemas.table["tmp"]["tmp_rfid_inv_non_rfid_loc_rls"]}_{batch}'

    SQL = f"""
    -- CREATE SALES DATA PARQUET TABLE
    CREATE TABLE IF NOT EXISTS {temp_temp_rfid_inv_non_rfid_loc} (LIKE {proto_temp_rfid_inv_non_rfid_loc});
    """
    return SQL


def pre_temp_rfid_inv_boh_create_tables_sql(batch, response_type):
    proto_temp_rfid_inv_boh = schemas.table['tmp_proto']['tmp_rfid_inv_boh']
    if response_type == constants.V_CHAIN_PFS:
        temp_temp_rfid_inv_boh = f'{schemas.table["tmp"]["tmp_rfid_inv_boh_pfs"]}_{batch}'
    elif response_type == constants.V_CHAIN_RLS:
        temp_temp_rfid_inv_boh = f'{schemas.table["tmp"]["tmp_rfid_inv_boh_rls"]}_{batch}'

    SQL = f"""
    -- CREATE SALES DATA PARQUET TABLE
    CREATE TABLE IF NOT EXISTS {temp_temp_rfid_inv_boh} (LIKE {proto_temp_rfid_inv_boh});
    """
    return SQL


def pre_temp_rfid_sls_inv_create_tables_sql(batch, response_type):
    proto_temp_rfid_sls_inv = schemas.table['tmp_proto']['tmp_rfid_sls_inv']
    if response_type == constants.V_CHAIN_PFS:
        temp_temp_rfid_sls_inv = f'{schemas.table["tmp"]["tmp_rfid_sls_inv_pfs"]}_{batch}'
    elif response_type == constants.V_CHAIN_RLS:
        temp_temp_rfid_sls_inv = f'{schemas.table["tmp"]["tmp_rfid_sls_inv_rls"]}_{batch}'

    SQL = f"""
    -- CREATE SALES DATA PARQUET TABLE
    CREATE TABLE IF NOT EXISTS {temp_temp_rfid_sls_inv} (LIKE {proto_temp_rfid_sls_inv});
    """
    return SQL


def pre_temp_rfid_room_sales_inventory_wrk_create_tables_sql(batch, response_type):
    proto_temp_rfid_room_sales_inventory_wrk = schemas.table[
        'tmp_proto']['tmp_rfid_room_sales_inventory_wrk']
    if response_type == constants.V_CHAIN_PFS:
        temp_temp_rfid_room_sales_inventory_wrk = f'{schemas.table["tmp"]["tmp_rfid_room_sales_inventory_wrk_pfs"]}_{batch}'
    elif response_type == constants.V_CHAIN_RLS:
        temp_temp_rfid_room_sales_inventory_wrk = f'{schemas.table["tmp"]["tmp_rfid_room_sales_inventory_wrk_rls"]}_{batch}'

    SQL = f"""
    -- CREATE SALES DATA PARQUET TABLE
    CREATE TABLE IF NOT EXISTS {temp_temp_rfid_room_sales_inventory_wrk} (LIKE {proto_temp_rfid_room_sales_inventory_wrk});
    """
    return SQL


def pre_temp_rfid_room_sales_inventory_create_tables_sql(batch, response_type):
    proto_temp_rfid_room_sales_inventory = schemas.table['tmp_proto']['tmp_rfid_room_sales_inventory']
    if response_type == constants.V_CHAIN_PFS:
        temp_temp_rfid_room_sales_inventory = f'{schemas.table["tmp"]["tmp_rfid_room_sales_inventory_pfs"]}_{batch}'
    elif response_type == constants.V_CHAIN_RLS:
        temp_temp_rfid_room_sales_inventory = f'{schemas.table["tmp"]["tmp_rfid_room_sales_inventory_rls"]}_{batch}'

    SQL = f"""
    -- CREATE SALES DATA PARQUET TABLE
    CREATE TABLE IF NOT EXISTS {temp_temp_rfid_room_sales_inventory} (LIKE {proto_temp_rfid_room_sales_inventory});
    """
    return SQL

# def pre_temp_create_demographic_dly_tables_sql(batch, response_type) :
#     proto_temp_f_store_demographic_hour_dly = schemas.table['tmp_proto']['f_store_demographic_hour_dly']
#     temp_temp_f_store_demographic_hour_dly = f'{schemas.table["tmp"]["f_store_demographic_hour_dly"]}_{batch}'

#     SQL = f"""
#     -- CREATE SALES DATA PARQUET TABLE
#     CREATE TABLE IF NOT EXISTS {temp_temp_f_store_demographic_hour_dly} (LIKE {proto_temp_f_store_demographic_hour_dly});
#     """
#     return SQL


def pre_stage_load_inv_parquet_from_landing_sql(batch, response_type):
    if response_type == 'Inventory':
        temp_stg_f_store_inventory_parquet = f'{schemas.table["stg_prefix"]["f_store_inventory_parquet"]}_{batch}'
        stg_landing_f_store_inventory_parquet = f"{schemas.table['stage_landing']['f_store_inventory_parquet']}"
        SQL = f"""
            INSERT INTO {temp_stg_f_store_inventory_parquet}
            SELECT * FROM {stg_landing_f_store_inventory_parquet}
            ;
        """
    return SQL


def temp_sq_inv_load_sql(batch):
    temp_temp_sq_inventory = f'{schemas.table["tmp"]["tmp_sq_inventory"]}_{batch}'
    temp_stg_f_store_inventory_parquet = f"{schemas.table['stg_prefix']['f_store_inventory_parquet']}_{batch}"
    SQL = f"""
        INSERT INTO {temp_temp_sq_inventory}
        SELECT * FROM {temp_stg_f_store_inventory_parquet}
        ;"""
    return SQL


def temp_scan_code_modified_load_sql(batch):
    temp_temp_scan_code_modified = f'{schemas.table["tmp"]["tmp_scan_code_modified"]}_{batch}'
    lu_scan_code = f"{schemas.table['lu']['d_scan_code']}"
    lu_regional_product = f"{schemas.table['lu']['d_regional_product']}"
    SQL = f"""
        INSERT INTO {temp_temp_scan_code_modified}
        (SELECT REGIONAL_PRODUCT_KEY,
        SCAN_ID,
        F.CHAIN_ID as CHAIN_ID
        FROM {lu_scan_code} F INNER JOIN {lu_regional_product} R
        ON R.CHAIN_ID = F.CHAIN_ID
        AND R.RTL_CLASS_ID = F.CLASS_ID
        AND R.rtl_vendororg_id = F.vendororg_id
        AND R.RTL_STYLE_ID = F.STYLE_ID
        AND R.RTL_COLOR_ID = F.COLOR_ID
        AND R.RTL_SIZE_ID = F.SIZE_ID
        WHERE F.SCANCODETYPE_CD= 'UPC')
        ;"""
    return SQL


def temp_exp_store_validation_load_sql(batch):
    temp_temp_exp_store_validation = f'{schemas.table["tmp"]["tmp_exp_store_validation"]}_{batch}'
    temp_temp_sq_inventory = f'{schemas.table["tmp"]["tmp_sq_inventory"]}_{batch}'
    lu_global_facility = f"{schemas.table['lu']['d_global_facility']}"

    SQL = f"""
        INSERT INTO {temp_temp_exp_store_validation}
        (SELECT LTRIM(RTRIM(REPLACE(SITENAME,'"',''))) as i_SITE_NAME,
        CASE WHEN LTRIM(RTRIM(SITENAME))='"Warehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"001-LBVWarehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"Warehouse-LasVegas"' THEN locationname
            ELSE LTRIM(RTRIM(REPLACE(SITENAME,'"',''))) END AS SITE_NAME,
        LTRIM(RTRIM(CASE WHEN LTRIM(RTRIM(SITENAME))='"Warehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"001-LBVWarehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"Warehouse-LasVegas"' THEN locationname
            ELSE LTRIM(RTRIM(REPLACE(SITENAME,'"',''))) END)) as FACILITY,
        cast(ltrim(rtrim(substring(CASE WHEN LTRIM(RTRIM(SITENAME))='"Warehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"001-LBVWarehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"Warehouse-LasVegas"' THEN locationname
            ELSE SITENAME END,2,REGEXP_INSTR(CASE WHEN LTRIM(RTRIM(SITENAME))='"Warehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"001-LBVWarehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"Warehouse-LasVegas"' THEN locationname
            ELSE SITENAME END,'-')-2))) as int) as FACILITY_ID,
        cast(ltrim(rtrim(substring(CASE WHEN LTRIM(RTRIM(SITENAME))='"Warehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"001-LBVWarehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"Warehouse-LasVegas"' THEN locationname
            ELSE SITENAME END,2,REGEXP_INSTR(CASE WHEN LTRIM(RTRIM(SITENAME))='"Warehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"001-LBVWarehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"Warehouse-LasVegas"' THEN locationname
            ELSE SITENAME END,'-')-2))) as int) as O_FACILITY_ID,
        LENGTH(CASE WHEN LTRIM(RTRIM(SITENAME))='"Warehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"001-LBVWarehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"Warehouse-LasVegas"' THEN locationname
            ELSE LTRIM(RTRIM(REPLACE(SITENAME,'"',''))) END) as length,
        cast(ltrim(rtrim(substring(CASE WHEN LTRIM(RTRIM(SITENAME))='"Warehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"001-LBVWarehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"Warehouse-LasVegas"' THEN locationname
            ELSE SITENAME END,REGEXP_INSTR(CASE WHEN LTRIM(RTRIM(SITENAME))='"Warehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"001-LBVWarehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"Warehouse-LasVegas"' THEN locationname
            ELSE SITENAME END,'-')+1,length(CASE WHEN LTRIM(RTRIM(SITENAME))='"Warehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"001-LBVWarehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"Warehouse-LasVegas"' THEN locationname
            ELSE SITENAME END)-REGEXP_INSTR(CASE WHEN LTRIM(RTRIM(SITENAME))='"Warehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"001-LBVWarehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"Warehouse-LasVegas"' THEN locationname
            ELSE SITENAME END,'-')-1))) as VARCHAR) as FACILITY_NAME,
        DIVISION,
        LTRIM(RTRIM(REPLACE(DIVISION,'"',''))) as O_DIVISION,
        SEASON,
        LTRIM(RTRIM(REPLACE(SEASON,'"',''))) as O_SEASON,
        DEPARTMENT,
        LTRIM(RTRIM(REPLACE(DEPARTMENT,'"',''))) as O_DEPARTMENT,
        VENDORSTYLE as VENDOR_STYLE,
        LTRIM(RTRIM(REPLACE(VENDORSTYLE,'"',''))) as O_VENDOR_STYLE,
        LTRIM(RTRIM(REPLACE(prodname,'"',''))) as PRODUCT_NAME,
        COLOR,
        LTRIM(RTRIM(REPLACE(COLOR,'"',''))) as O_COLOR,
        SIZE,
        LTRIM(RTRIM(REPLACE(SIZE,'"',''))) as O_SIZE,
        UPC,
        CASE WHEN LENGTH(REPLACE(UPC,'"',''))>1 THEN
                substring('00000000000000000000',1,20-LENGTH(LTRIM(RTRIM(REPLACE(UPC,'"','')))))||LTRIM(RTRIM(REPLACE(UPC,'"','')))
            ELSE REPLACE(UPC,'"','') END AS SCAN_ID,
        REPLACE(locationname,'"','') as LOCATION,
        REPLACE(CURRENTINVENTORY,'"','') as CURRENT_INVENTORY,
        CASE WHEN CURRENTINVENTORY is Null THEN null
            ELSE cast(REPLACE(CURRENTINVENTORY,'"','') AS INT) END AS O_CURRENT_INVENTORY,
        REPLACE(lin,'"','') as LONG_ITEM_ID,
        LKP.CHAIN_ID as CHAIN_ID
        FROM {temp_temp_sq_inventory} STG
        LEFT JOIN {lu_global_facility} LKP
        ON LKP.RTL_FACILITY_CD = cast(ltrim(rtrim(substring(CASE WHEN LTRIM(RTRIM(SITENAME))='"Warehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"001-LBVWarehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"Warehouse-LasVegas"' THEN locationname
            ELSE SITENAME END,2,REGEXP_INSTR(CASE WHEN LTRIM(RTRIM(SITENAME))='"Warehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"001-LBVWarehouse"' THEN locationname
            WHEN LTRIM(RTRIM(SITENAME))='"Warehouse-LasVegas"' THEN locationname
            ELSE SITENAME END,'-')-2))) as int))
        ;"""
    return SQL


def temp_rtr_store_map_exception_load_sql(batch):
    temp_temp_rtr_store_map_exception = f'{schemas.table["tmp"]["tmp_rtr_store_map_exception"]}_{batch}'
    temp_temp_exp_store_validation = f'{schemas.table["tmp"]["tmp_exp_store_validation"]}_{batch}'
    temp_temp_scan_code_modified = f'{schemas.table["tmp"]["tmp_scan_code_modified"]}_{batch}'
    SQL = f"""
        INSERT INTO {temp_temp_rtr_store_map_exception} 
        (select I_SITE_NAME AS SITE_NAME,
        INV.CHAIN_ID as CHAIN_ID,
        O_FACILITY_ID AS RTL_FACILITY_CD,
        FACILITY_NAME,
        O_DIVISION,
        O_SEASON,
        O_DEPARTMENT,
        O_VENDOR_STYLE,
        PRODUCT_NAME,
        O_COLOR,
        O_SIZE,
        INV.SCAN_ID AS SCAN_ID,
        LOCATION,
        O_CURRENT_INVENTORY AS CURRENT_INVENTORY,
        LONG_ITEM_ID,
        LKP.SCAN_ID AS SCAN_ID_LKP,
        LKP.CHAIN_ID AS CHAIN_ID_LKP,
        REGIONAL_PRODUCT_KEY
        FROM {temp_temp_exp_store_validation} INV
        LEFT JOIN {temp_temp_scan_code_modified} LKP
        ON INV.SCAN_ID = LKP.SCAN_ID AND INV.CHAIN_ID = LKP.CHAIN_ID)
        ;"""
    return SQL


def temp_rfid_store_inventory_stg_load_sql(batch):
    temp_temp_rfid_store_inventory_stg = f'{schemas.table["tmp"]["tmp_rfid_store_inventory_stg"]}_{batch}'
    temp_temp_rtr_store_map_exception = f'{schemas.table["tmp"]["tmp_rtr_store_map_exception"]}_{batch}'
    SQL = f"""
        INSERT INTO {temp_temp_rfid_store_inventory_stg} 
        (SELECT CHAIN_ID,
        RTL_FACILITY_CD,
        FACILITY_NAME,
        O_DIVISION as DIVISION,
        O_SEASON as SEASON,
        O_DEPARTMENT as DEPARTMENT,
        O_VENDOR_STYLE as VENDORSTYLE,
        PRODUCT_NAME,
        O_COLOR as COLOR,
        O_SIZE as SIZE,
        SCAN_ID as SCAN_ID,
        REGIONAL_PRODUCT_KEY,
        LOCATION,
        CURRENT_INVENTORY as INVENTORY_QTY,
        LONG_ITEM_ID as LONGITEM_ID
        FROM {temp_temp_rtr_store_map_exception} WHERE CHAIN_ID IS NOT null)
        ;"""
    return SQL


def temp_rfid_exception_load_sql(batch):
    temp_temp_rfid_exception = f'{schemas.table["tmp"]["tmp_rfid_exception"]}_{batch}'
    temp_temp_rtr_store_map_exception = f'{schemas.table["tmp"]["tmp_rtr_store_map_exception"]}_{batch}'
    SQL = f"""
        INSERT INTO {temp_temp_rfid_exception} 
        (SELECT 10 AS EXCEPTION_ID,
        'RTL_FACILITY_CD MISSING IN FACILITY TABLE' as EXCEPTION_DESC,
        CAST(COALESCE(CHAIN_ID,0) AS VARCHAR) AS CHAIN_ID,
        CAST(COALESCE(SCAN_ID,'') AS VARCHAR) AS SCAN_ID,
        CAST(0 AS BIGINT) as REGIONAL_PRODUCT_KEY,
        CAST(COALESCE(O_DEPARTMENT,'0') AS VARCHAR) AS RTL_DEPT_ID,
        0 as RTL_CLASS_ID,
        CAST(COALESCE(O_COLOR,'0') AS VARCHAR) AS RTL_COLOR_ID,
        CAST(COALESCE(O_SIZE,'0') AS VARCHAR) AS RTL_SIZE_ID,
        CAST(COALESCE(RTL_FACILITY_CD,0) AS VARCHAR) AS RTL_FACILITY_CD,
        'STORE_GROUP' AS STORE_GROUP,
        CAST(COALESCE(SITE_NAME,'') AS VARCHAR) AS CLASSVENDORSTYLE_CD,
        CURRENT_TIMESTAMP AS CREATE_TS,
        CURRENT_TIMESTAMP AS UPDATE_TS from {temp_temp_rtr_store_map_exception} where CHAIN_ID IS NULL)
        ;"""
    return SQL


def temp_rfid_missing_inventory_stg_load_sql(batch):
    temp_temp_rfid_missing_inventory_stg = f'{schemas.table["tmp"]["tmp_rfid_missing_inventory_stg"]}_{batch}'
    temp_temp_rtr_store_map_exception = f'{schemas.table["tmp"]["tmp_rtr_store_map_exception"]}_{batch}'
    SQL = f"""
        INSERT INTO {temp_temp_rfid_missing_inventory_stg} 
        (SELECT CHAIN_ID
        ,RTL_FACILITY_CD
        ,FACILITY_NAME
        ,SCAN_ID
        ,REGIONAL_PRODUCT_KEY
        ,PRODUCT_NAME
        ,CASE WHEN SCAN_ID_LKP IS NULL  THEN 'UPC MISSING IN DSS DATABASE'
            ELSE (CASE WHEN CHAIN_ID <> CHAIN_ID_LKP THEN 'UPC BELONGS TO ANOTHER CHAIN'
                    ELSE 'UPC AVAILABLE IN DSS' END) END AS ERROR_DESC
        ,O_DIVISION AS DIVISION
        ,O_SEASON AS SEASON
        ,O_DEPARTMENT AS DEPARTMENT
        ,O_VENDOR_STYLE AS VENDORSTYLE
        ,O_COLOR AS COLOR
        ,O_SIZE AS SIZE
        ,LOCATION
        ,CURRENT_INVENTORY AS INVENTORY_QTY
        ,LONG_ITEM_ID AS LONGITEM_ID
        from {temp_temp_rtr_store_map_exception} where
        (LTRIM(RTRIM(PRODUCT_NAME))='unknown product' OR LTRIM(RTRIM(PRODUCT_NAME))='UNLINKED TAG')
        AND NOT(LTRIM(RTRIM(SCAN_ID))='') AND CHAIN_ID IS NOT null)
        ;"""
    return SQL


def temp_inventory_merge_load_sql(batch):
    temp_temp_inventory_merge = f'{schemas.table["tmp"]["tmp_inventory_merge"]}_{batch}'
    temp_temp_rfid_store_inventory_stg = f'{schemas.table["tmp"]["tmp_rfid_store_inventory_stg"]}_{batch}'
    lu_rfid_location_xref = f"{schemas.table['lu']['d_rfid_location_xref']}"
    SQL = f"""
        INSERT INTO {temp_temp_inventory_merge}
        (SELECT  FLAG,CHAIN_ID,RTL_FACILITY_CD,FACILITY_NAME,SCAN_ID,REGIONAL_PRODUCT_KEY,LONGITEM_ID,PRODUCT_NAME,LOCATION,LOCATION_CATEGORY,INVENTORY_QTY
        FROM (SELECT
            'NOT EXISTS' AS FLAG,
            INVENTORY.CHAIN_ID,
            INVENTORY.RTL_FACILITY_CD,
            INVENTORY.FACILITY_NAME,
            INVENTORY.SCAN_ID,
            INVENTORY.REGIONAL_PRODUCT_KEY,
            INVENTORY.LONGITEM_ID,
            INVENTORY.PRODUCT_NAME,
            INVENTORY.LOCATION,
            NVL(LOCATION_XREF.LOCATION_CATEGORY,'NA') LOCATION_CATEGORY,
            INVENTORY.INVENTORY_QTY
            FROM {temp_temp_rfid_store_inventory_stg} INVENTORY
            LEFT JOIN {lu_rfid_location_xref} LOCATION_XREF
            ON INVENTORY.LOCATION = LOCATION_XREF.LOCATION
    WHERE   LTRIM(RTRIM(INVENTORY.SCAN_ID))<>'') STR_INV_MRG
    GROUP   BY 1,2,3,4,5,6,7,8,9,10,11)
        ;"""
    return SQL


def temp_missing_inventory_merge_alter_CALC_INV_QTY_sql(batch):
    temp_temp_inventory_merge = f'{schemas.table["tmp"]["tmp_inventory_merge"]}_{batch}'
    SQL = f"""
         alter table {temp_temp_inventory_merge} add 
         column CALC_INV_QTY int default NULL
        ;"""
    return SQL


def temp_missing_inventory_merge_alter_CC_IND_sql(batch):
    temp_temp_inventory_merge = f'{schemas.table["tmp"]["tmp_inventory_merge"]}_{batch}'
    SQL = f"""
         alter table {temp_temp_inventory_merge} add 
         column CC_IND varchar default NULL
        ;"""
    return SQL


def tgt_rfid_store_inventory_load_sql(batch, load_date):
    tgt_rfid_store_inventory = f'{schemas.table["tgt"]["rfid_store_inventory"]}'
    temp_temp_inventory_merge = f'{schemas.table["tmp"]["tmp_inventory_merge"]}_{batch}'
    lu_global_facility = f"{schemas.table['lu']['d_global_facility']}"
    SQL = f"""
        INSERT INTO {tgt_rfid_store_inventory}
        (SELECT CAST(INVENTORY_MERGE.CHAIN_ID as INT) as chain_id,
            CAST(GLOBAL_FACILITY.GLOBAL_FACILITY_KEY as BIGINT) as global_facility_key,
            INVENTORY_MERGE.FACILITY_NAME as facility_name,
            INVENTORY_MERGE.SCAN_ID as scan_id,
            INVENTORY_MERGE.REGIONAL_PRODUCT_KEY as regional_product_key,
            INVENTORY_MERGE.LONGITEM_ID as longitem_id,
            INVENTORY_MERGE.PRODUCT_NAME as product_name,
            INVENTORY_MERGE.LOCATION as location,
            INVENTORY_MERGE.LOCATION_CATEGORY as location_category,
            INVENTORY_MERGE.INVENTORY_QTY as inventory_qty,
            CALC_INV_QTY as calc_inv_qty,
            '{load_date}' as load_date,
            CC_IND as cc_ind,
            CURRENT_TIMESTAMP AS create_ts,
            CURRENT_TIMESTAMP AS update_ts,
            CASE WHEN CAST(INVENTORY_MERGE.CHAIN_ID AS INT)=3 THEN 2
                    WHEN CAST(INVENTORY_MERGE.CHAIN_ID AS INT)=5 THEN 1
                    WHEN CAST(INVENTORY_MERGE.CHAIN_ID AS INT)=1 THEN 3
                ELSE -1 END AS source_id
            FROM {temp_temp_inventory_merge} INVENTORY_MERGE LEFT JOIN
            {lu_global_facility} GLOBAL_FACILITY ON INVENTORY_MERGE.RTL_FACILITY_CD=GLOBAL_FACILITY.RTL_FACILITY_CD
            WHERE FLAG='NOT EXISTS')
        ;""".format(load_date=load_date)
    return SQL


def temp_missing_inventory_merge_load_sql(batch):
    temp_temp_missing_inventory_merge = f'{schemas.table["tmp"]["tmp_missing_inventory_merge"]}_{batch}'
    temp_temp_rfid_missing_inventory_stg = f'{schemas.table["tmp"]["tmp_rfid_missing_inventory_stg"]}_{batch}'
    lu_global_facility = f"{schemas.table['lu']['d_global_facility']}"
    SQL = f"""
        INSERT INTO {temp_temp_missing_inventory_merge}
        (SELECT  CHAIN_ID, GLOBAL_FACILITY_KEY, FACILITY_NAME, SCAN_ID, REGIONAL_PRODUCT_KEY, LONGITEM_ID, 
            PRODUCT_NAME, ERROR_DESC, LOCATION, INVENTORY_QTY
        FROM (SELECT INVENTORY.CHAIN_ID
        , GLOBAL_FACILITY.GLOBAL_FACILITY_KEY
        , INVENTORY.FACILITY_NAME
        , INVENTORY.SCAN_ID
        , INVENTORY.REGIONAL_PRODUCT_KEY
        , INVENTORY.LONGITEM_ID
        , INVENTORY.PRODUCT_NAME
        , INVENTORY.ERROR_DESC
        , INVENTORY.LOCATION
        , INVENTORY.INVENTORY_QTY
        FROM   {temp_temp_rfid_missing_inventory_stg} INVENTORY LEFT JOIN
            {lu_global_facility} GLOBAL_FACILITY ON INVENTORY.RTL_FACILITY_CD=GLOBAL_FACILITY.RTL_FACILITY_CD
    WHERE   LTRIM(RTRIM(INVENTORY.SCAN_ID)) <> '') MIS_INV_MRG
    GROUP   BY 1,2,3,4,5,6,7,8,9,10)
        ;"""
    return SQL


def tgt_rfid_missing_inventory_update_120_days_sql(batch):
    tgt_rfid_missing_inventory = f'{schemas.table["tgt"]["rfid_missing_inventory"]}'
    SQL = f"""
    delete from {tgt_rfid_missing_inventory}
    where update_ts>= current_date - interval '120 day'
    ;"""
    return SQL


def tgt_rfid_missing_inventory_load_sql(batch):
    tgt_rfid_missing_inventory = f'{schemas.table["tgt"]["rfid_missing_inventory"]}'
    temp_temp_missing_inventory_merge = f'{schemas.table["tmp"]["tmp_missing_inventory_merge"]}_{batch}'
    SQL = f"""
        INSERT INTO {tgt_rfid_missing_inventory}
        (SELECT CAST(CHAIN_ID AS INT) AS CHAIN_ID
        , GLOBAL_FACILITY_KEY
        , FACILITY_NAME
        , SCAN_ID
        , REGIONAL_PRODUCT_KEY
        , LONGITEM_ID
        , PRODUCT_NAME
        , ERROR_DESC
        , LOCATION
        , INVENTORY_QTY
        , CURRENT_DATE AS LOAD_DATE
        , CURRENT_TIMESTAMP AS CREATE_TS
        , CURRENT_TIMESTAMP AS UPDATE_TS
        FROM {temp_temp_missing_inventory_merge})
        ;"""
    return SQL


def temp_rfid_location_xref_load_sql(batch, load_date, chain_details):
    V_CHAIN = chain_details.get('V_CHAIN')
    floor = chain_details.get('floor')
    chain_id = chain_details.get('chain_id')
    lu_d_rfid_store_inventory = f'{schemas.table["lu"]["d_rfid_store_inventory"]}'
    lu_d_rfid_store_tgt_cpty = f'{schemas.table["lu"]["d_rfid_store_tgt_cpty"]}'
    if V_CHAIN == constants.V_CHAIN_PFS:
        temp_temp_rfid_location_xref = f'{schemas.table["tmp"]["tmp_rfid_location_xref_pfs"]}_{batch}'
    elif V_CHAIN == constants.V_CHAIN_RLS:
        temp_temp_rfid_location_xref = f'{schemas.table["tmp"]["tmp_rfid_location_xref_rls"]}_{batch}'
    SQL = f"""
        Insert into {temp_temp_rfid_location_xref}
        (select distinct CHAIN_iD
            ,GLOBAL_FACILITY_KEY
            ,CLASS_VENDOR_STYLE
            ,location as location
        from (SELECT CHAIN_iD
                    ,GLOBAL_FACILITY_KEY
                    ,split_part(longitem_Id,'-',1)|| '-'||split_part(longitem_Id,'-',2)|| '-'||split_part(longitem_Id,'-',3)  as CLASS_VENDOR_STYLE
                    ,case when upper(location) like '%BACK%' then 'BACK OF HOUSE'
                        when upper(location) like '%ROOM 1%' OR upper(location) like '%FIRST%' OR upper(location) like '%ROOM 10%' then 'SALES FLOOR'
                        when upper(location) like '%ROOM 2%' then 'SALES FLOOR 2'
                        when upper(location) like '%ROOM 3%' then 'SALES FLOOR 3'
                        when upper(location) like '%ROOM 4%' then 'SALES FLOOR 4'
                        when upper(location) like '%ROOM 5%' then 'SALES FLOOR 5'
                        when upper(location) like '%ROOM 6%' then 'SALES FLOOR 6'
                        when upper(location) like '%ROOM 7%' then 'SALES FLOOR 7'
                        when upper(location) like '%ROOM 8%' then 'SALES FLOOR 8'
                        when upper(location) like '%ROOM 9%' then 'SALES FLOOR 9' ELSE upper(location) end as LOCATION
                    ,RANK() OVER (PARTITION BY CHAIN_ID,GLOBAL_FACILITY_KEY,LOAD_DATE,split_part(longitem_Id,'-',1)|| '-'||split_part(longitem_Id,'-',2)|| '-'||split_part(longitem_Id,'-',3) ORDER BY inventory_qty DESC) AS RNK
            FROM {lu_d_rfid_store_inventory} INV
            WHERE LOAD_DATE='{load_date}'
--            AND (UPPER(LOCATION) LIKE '{floor}')
            AND CHAIN_ID = {chain_id}
            and REGIONAL_PRODUCT_KEY IS NOT NULL
            and EXISTS (SELECT 1 FROM {lu_d_rfid_store_tgt_cpty} WHERE CHAIN_ID=3 AND RFID_STORE_TGT_CPTY.GLOBAL_FACILITY_KEY=INV.GLOBAL_FACILITY_KEY))A
        where A.rnk=1)
        ;""".format(chain_id=chain_id, load_date=load_date, floor=floor)
    return SQL


def temp_rfid_room_inv_temp_load_sql(batch, load_date, chain_details):
    V_CHAIN = chain_details.get('V_CHAIN')
    wh = chain_details.get('wh')
    bh = chain_details.get('bh')
    chain_id = chain_details.get('chain_id')
    lu_d_rfid_store_inventory = f'{schemas.table["lu"]["d_rfid_store_inventory"]}'
    lu_d_rfid_store_tgt_cpty = f'{schemas.table["lu"]["d_rfid_store_tgt_cpty"]}'
    if V_CHAIN == constants.V_CHAIN_PFS:
        temp_temp_rfid_room_inv_temp = f'{schemas.table["tmp"]["tmp_rfid_room_inv_temp_pfs"]}_{batch}'
    elif V_CHAIN == constants.V_CHAIN_RLS:
        temp_temp_rfid_room_inv_temp = f'{schemas.table["tmp"]["tmp_rfid_room_inv_temp_rls"]}_{batch}'
    SQL = f"""
        Insert into {temp_temp_rfid_room_inv_temp}
        (SELECT CHAIN_ID
            ,GLOBAL_FACILITY_KEY
            ,LOAD_DATE
            ,REGIONAL_PRODUCT_KEY
            ,split_part(longitem_Id,'-',1)|| '-'||split_part(longitem_Id,'-',2)|| '-'||split_part(longitem_Id,'-',3)  as CLASS_VENDOR_STYLE
            ,LOCATION
            ,INVENTORY_QTY
            ,RANK() OVER (PARTITION BY CHAIN_ID,GLOBAL_FACILITY_KEY,LOAD_DATE,REGIONAL_PRODUCT_KEY ORDER BY location DESC)AS RNK
        FROM(SELECT CHAIN_ID
                    ,GLOBAL_FACILITY_KEY
                    ,LOAD_DATE
                    ,REGIONAL_PRODUCT_KEY
                    ,longitem_id
                    ,case when upper(location) like '%BACK%' then 'BACK OF HOUSE'
                        when upper(location) like '%ROOM 1%' OR upper(location) like '%FIRST%' OR upper(location) like '%ROOM 10%' then 'SALES FLOOR'
                        when upper(location) like '%ROOM 2%' then 'SALES FLOOR 2'
                        when upper(location) like '%ROOM 3%' then 'SALES FLOOR 3'
                        when upper(location) like '%ROOM 4%' then 'SALES FLOOR 4'
                        when upper(location) like '%ROOM 5%' then 'SALES FLOOR 5'
                        when upper(location) like '%ROOM 6%' then 'SALES FLOOR 6'
                        when upper(location) like '%ROOM 7%' then 'SALES FLOOR 7'
                        when upper(location) like '%ROOM 8%' then 'SALES FLOOR 8'
                        when upper(location) like '%ROOM 9%' then 'SALES FLOOR 9' ELSE upper(location) end as LOCATION
                        ,INVENTORY_QTY
                FROM {lu_d_rfid_store_inventory} RFID_STORE_INVENTORY
                WHERE EXISTS (SELECT 1
                                    FROM {lu_d_rfid_store_tgt_cpty} RFID_STORE_TGT_CPTY
                                    WHERE CHAIN_ID={chain_id}
                                    AND RFID_STORE_INVENTORY.GLOBAL_FACILITY_KEY=RFID_STORE_TGT_CPTY.GLOBAL_FACILITY_KEY)
                    AND LOAD_DATE={load_date}
                    AND UPPER(LOCATION) NOT LIKE '{wh}'
                    AND REGIONAL_PRODUCT_KEY IS NOT NULL)A
        WHERE UPPER(LOCATION) NOT LIKE '{bh}'
        UNION
        SELECT CHAIN_ID
            ,GLOBAL_FACILITY_KEY
            ,LOAD_DATE
            ,REGIONAL_PRODUCT_KEY
            ,split_part(longitem_Id,'-',1)|| '-'||split_part(longitem_Id,'-',2)|| '-'||split_part(longitem_Id,'-',3) as CLASS_VENDOR_STYLE
            ,case when upper(location) like '%BACK%' then 'BACK OF HOUSE' else UPPER(LOCATION) end AS LOCATION
            ,INVENTORY_QTY
            ,0 as rnk
        FROM {lu_d_rfid_store_inventory} RFID_STORE_INVENTORY
        WHERE EXISTS (SELECT 1
                                FROM {lu_d_rfid_store_tgt_cpty} RFID_STORE_TGT_CPTY
                                WHERE CHAIN_ID={chain_id}
                                AND RFID_STORE_TGT_CPTY.GLOBAL_FACILITY_KEY=RFID_STORE_INVENTORY.GLOBAL_FACILITY_KEY)
            AND LOAD_DATE={load_date}
            AND UPPER(LOCATION) NOT LIKE '{wh}'
            AND REGIONAL_PRODUCT_KEY IS NOT NULL
            AND (UPPER(LOCATION) LIKE '{bh}'))
        ;""".format(chain_id=chain_id, load_date=load_date, wh=wh, bh=bh)
    return SQL


def temp_rfid_inv_non_rfid_load_sql(batch, load_date, chain_details):
    V_CHAIN = chain_details.get('V_CHAIN')
    lu_d_fact_sellout_daily = f'{schemas.table["lu"]["d_fact_sellout_daily"]}'
    if V_CHAIN == constants.V_CHAIN_PFS:
        temp_temp_rfid_inv_non_rfid = f'{schemas.table["tmp"]["tmp_rfid_inv_non_rfid_pfs"]}_{batch}'
        temp_temp_rfid_room_inv_temp = f'{schemas.table["tmp"]["tmp_rfid_room_inv_temp_pfs"]}_{batch}'
    elif V_CHAIN == constants.V_CHAIN_RLS:
        temp_temp_rfid_inv_non_rfid = f'{schemas.table["tmp"]["tmp_rfid_inv_non_rfid_rls"]}_{batch}'
        temp_temp_rfid_room_inv_temp = f'{schemas.table["tmp"]["tmp_rfid_room_inv_temp_rls"]}_{batch}'
    SQL = f"""
        Insert into {temp_temp_rfid_inv_non_rfid}
        (SELECT PD.GLOBAL_FACILITY_KEY
                ,PD.PERIODEND_DT
                ,PD.REGIONAL_PRODUCT_KEY
                ,sum(nvl(net_sls_qty,0)) as sales_qty
                ,NVL(sum(NET_SLS_COST_AMT),0.000000) AS SALESCOST_AMT
                ,sum(NVL(NET_SLS_RTL_EXVAT_AMT,0)) AS SALESRETAIL_AMT
            FROM {lu_d_fact_sellout_daily} PD
            LEFT JOIN {temp_temp_rfid_room_inv_temp} RF
                ON PD.GLOBAL_FACILITY_KEY=RF.GLOBAL_FACILITY_KEY
                AND PD.REGIONAL_PRODUCT_KEY=RF.REGIONAL_PRODUCT_KEY
                AND PD.PERIODEND_DT=RF.LOAD_DATE
            WHERE PD.PERIODEND_DT={load_date}
                AND RF.REGIONAL_PRODUCT_KEY IS NULL
            GROUP BY PD.GLOBAL_FACILITY_KEY,PD.PERIODEND_DT,PD.REGIONAL_PRODUCT_KEY)
        ;""".format(load_date=load_date)
    return SQL


def temp_rfid_inv_non_rfid_loc_pfs_load_sql(batch, load_date, chain_details):
    temp_temp_rfid_inv_non_rfid = f'{schemas.table["tmp"]["tmp_rfid_inv_non_rfid_pfs"]}_{batch}'
    temp_temp_rfid_location_xref = f'{schemas.table["tmp"]["tmp_rfid_location_xref_pfs"]}_{batch}'
    lu_d_global_facility = f'{schemas.table["lu"]["d_global_facility"]}'
    lu_d_regional_product_outer = f'{schemas.table["lu"]["d_regional_product_outer"]}'
    temp_temp_rfid_inv_non_rfid_loc = f'{schemas.table["tmp"]["tmp_rfid_inv_non_rfid_loc_pfs"]}_{batch}'
    SQL = f"""
        Insert into {temp_temp_rfid_inv_non_rfid_loc}
        (select f.chain_id
                ,A.GLOBAL_FACILITY_KEY
                ,PERIODEND_DT
                ,A.REGIONAL_PRODUCT_KEY
                ,NVL(SALES_QTY,0) AS SALES_QTY
                ,0 AS INVENTORY_QTY
                ,split_part(longitem_Id,'-',1)|| '-'||split_part(longitem_Id,'-',2)|| '-'||split_part(longitem_Id,'-',3) as class_vendor_style
                ,NVL(location,'NON-RFID') as location
            FROM {temp_temp_rfid_inv_non_rfid}	 A
            JOIN {lu_d_global_facility} F
                ON A.GLOBAL_FACILITY_KEY=F.GLOBAL_FACILITY_KEY
            JOIN {lu_d_regional_product_outer} I
                ON I.REGIONAL_PRODUCT_KEY=A.REGIONAL_PRODUCT_KEY
            LEFT JOIN {temp_temp_rfid_location_xref} B
                ON A.GLOBAL_FACILITY_KEY=B.GLOBAL_FACILITY_KEY
                and B.class_vendor_style=split_part(longitem_Id,'-',1)|| '-'||split_part(longitem_Id,'-',2)|| '-'||split_part(longitem_Id,'-',3))
        ;"""
    return SQL


def temp_rfid_inv_non_rfid_loc_rls_load_sql(batch, load_date, chain_details):
    temp_temp_rfid_inv_non_rfid = f'{schemas.table["tmp"]["tmp_rfid_inv_non_rfid_rls"]}_{batch}'
    lu_d_regional_product_outer = f'{schemas.table["lu"]["d_regional_product_outer"]}'
    lu_d_rls_display_item_xref = f'{schemas.table["lu"]["d_rls_display_item_xref"]}'
    temp_temp_rfid_inv_non_rfid_loc = f'{schemas.table["tmp"]["tmp_rfid_inv_non_rfid_loc_rls"]}_{batch}'
    SQL = f"""
         insert into {temp_temp_rfid_inv_non_rfid_loc}               
        (SELECT I.chain_Id
                ,R.GLOBAL_FACILITY_KEY
                ,PERIODEND_DT
                ,R.REGIONAL_PRODUCT_KEY
                ,NVL(SALES_QTY,0) AS SALES_QTY
                ,0 as INVENTORY_QTY
                ,split_part(longitem_Id,'-',1)|| '-'||split_part(longitem_Id,'-',2)|| '-'||split_part(longitem_Id,'-',3) as class_vendor_style
                ,NVL(XREF.LOCATION,'NON-RFID') AS LOCATION
            FROM {temp_temp_rfid_inv_non_rfid}	 R
            JOIN {lu_d_regional_product_outer} I
                ON I.REGIONAL_PRODUCT_KEY=R.REGIONAL_PRODUCT_KEY
            LEFT JOIN {lu_d_rls_display_item_xref} XREF
                ON R.GLOBAL_FACILITY_KEY=XREF.STORE_ID
                AND I.DEPT_ID=XREF.DEPARTMENT)
        ;"""
    return SQL


def temp_rfid_inv_boh_load_sql(batch, load_date, chain_details):
    V_CHAIN = chain_details.get('V_CHAIN')
    if V_CHAIN == constants.V_CHAIN_PFS:
        temp_temp_rfid_room_inv_temp = f'{schemas.table["tmp"]["tmp_rfid_room_inv_temp_pfs"]}_{batch}'
        temp_temp_rfid_location_xref = f'{schemas.table["tmp"]["tmp_rfid_location_xref_pfs"]}_{batch}'
        temp_temp_rfid_inv_non_rfid_loc = f'{schemas.table["tmp"]["tmp_rfid_inv_non_rfid_loc_pfs"]}_{batch}'
        temp_temp_rfid_inv_boh = f'{schemas.table["tmp"]["tmp_rfid_inv_boh_pfs"]}_{batch}'
    elif V_CHAIN == constants.V_CHAIN_RLS:
        temp_temp_rfid_room_inv_temp = f'{schemas.table["tmp"]["tmp_rfid_room_inv_temp_rls"]}_{batch}'
        temp_temp_rfid_location_xref = f'{schemas.table["tmp"]["tmp_rfid_location_xref_rls"]}_{batch}'
        temp_temp_rfid_inv_non_rfid_loc = f'{schemas.table["tmp"]["tmp_rfid_inv_non_rfid_loc_rls"]}_{batch}'
        temp_temp_rfid_inv_boh = f'{schemas.table["tmp"]["tmp_rfid_inv_boh_rls"]}_{batch}'
    SQL = f"""
        Insert into {temp_temp_rfid_inv_boh}
        (select CHAIN_ID
                ,GLOBAL_FACILITY_KEY
                ,load_date
                ,REGIONAL_PRODUCT_KEY
                ,LOCATION
                ,inventory_qty
                ,rank() over (partition by CHAIN_ID,GLOBAL_FACILITY_KEY,load_date,REGIONAL_PRODUCT_KEY order by inventory_qty desc ,location desc) as rnk
            from (select CHAIN_ID
                        ,GLOBAL_FACILITY_KEY
                        ,load_date
                        ,REGIONAL_PRODUCT_KEY
                        ,LOCATION
                        ,sum(INVENTORY_QTY) as inventory_qty
                    from(select a.CHAIN_ID
                                ,a.GLOBAL_FACILITY_KEY
                                ,LOAD_DATE
                                ,a.REGIONAL_PRODUCT_KEY
                                ,nvl(b.LOCATION,'NON-RFID') as location
                                ,0 as INVENTORY_QTY
                            from {temp_temp_rfid_room_inv_temp} a
                            left join {temp_temp_rfid_location_xref} b
                                on a.CHAIN_ID=b.chain_id
                                and a.GLOBAL_FACILITY_KEY=b.GLOBAL_FACILITY_KEY
                                and a.class_vendor_style=b.class_vendor_style
                            where rnk=0
                        union
                        select CHAIN_ID
                                ,GLOBAL_FACILITY_KEY
                                ,LOAD_DATE
                                ,REGIONAL_PRODUCT_KEY
                                ,LOCATION as location
                                ,INVENTORY_QTY
                            from {temp_temp_rfid_room_inv_temp}
                            where rnk>0
                        union
                        select CHAIN_ID
                                ,GLOBAL_FACILITY_KEY
                                ,PERIODEND_DT as load_date
                                ,REGIONAL_PRODUCT_KEY
                                ,LOCATION
                                ,0 as INVENTORY_QTY
                            from {temp_temp_rfid_inv_non_rfid_loc}
                        )INV
                    group by CHAIN_ID,GLOBAL_FACILITY_KEY,load_date,REGIONAL_PRODUCT_KEY,LOCATION) R1)
        ;"""
    return SQL


def temp_rfid_sls_inv_load_sql(batch, load_date, chain_details):
    V_CHAIN = chain_details.get('V_CHAIN')
    lu_d_fact_sellout_daily = f'{schemas.table["lu"]["d_fact_sellout_daily"]}'
    if V_CHAIN == constants.V_CHAIN_PFS:
        temp_temp_rfid_inv_boh = f'{schemas.table["tmp"]["tmp_rfid_inv_boh_pfs"]}_{batch}'
        temp_temp_rfid_sls_inv = f'{schemas.table["tmp"]["tmp_rfid_sls_inv_pfs"]}_{batch}'
    elif V_CHAIN == constants.V_CHAIN_RLS:
        temp_temp_rfid_inv_boh = f'{schemas.table["tmp"]["tmp_rfid_inv_boh_rls"]}_{batch}'
        temp_temp_rfid_sls_inv = f'{schemas.table["tmp"]["tmp_rfid_sls_inv_rls"]}_{batch}'
    SQL = f"""
        insert into {temp_temp_rfid_sls_inv}                   
        (select CHAIN_ID
                ,GLOBAL_FACILITY_KEY
                ,REGIONAL_PRODUCT_KEY
                ,LOAD_DATE
                ,LOCATION
                ,INVENTORY_QTY
                ,nvl(SALES_QTY,0) as sales_qty
                ,SALESCOST_AMT as SALESCOST_AMT
                ,nvl(SALESRETAIL_AMT,0) as SALESRETAIL_AMT
                ,nvl(SALESRETAIL_AMT-SALESCOST_AMT,0) as GM
            from(select CHAIN_ID
                        ,b.GLOBAL_FACILITY_KEY
                        ,b.REGIONAL_PRODUCT_KEY
                        ,LOAD_DATE
                        ,LOCATION
                        ,INVENTORY_QTY
                        ,sum(nvl(net_sls_qty,0)) as sales_qty
                        ,NVL(sum(NET_SLS_COST_AMT),0.000000) AS SALESCOST_AMT
                        ,sum(NVL(NET_SLS_RTL_EXVAT_AMT,0)) AS SALESRETAIL_AMT
                        ,nvl(sum(NVL(NET_SLS_RTL_EXVAT_AMT,0))-NVL(sum(NET_SLS_COST_AMT),0.000000),0) as GM
                    from {temp_temp_rfid_inv_boh} b
                    join {lu_d_fact_sellout_daily} inv
                        on b.GLOBAL_FACILITY_KEY=inv.GLOBAL_FACILITY_KEY
                        and b.REGIONAL_PRODUCT_KEY=inv.REGIONAL_PRODUCT_KEY
                        and b.load_date=inv.periodend_dt
                    where b.rnk=1
                    group by CHAIN_ID,b.GLOBAL_FACILITY_KEY,b.REGIONAL_PRODUCT_KEY,LOAD_DATE,LOCATION,INVENTORY_QTY
                union
                select CHAIN_ID
                        ,GLOBAL_FACILITY_KEY
                        ,REGIONAL_PRODUCT_KEY
                        ,LOAD_DATE
                        ,LOCATION
                        ,INVENTORY_QTY
                        ,0 as SALES_QTY
                        ,0 as SALESCOST_AMT
                        ,0 as SALESRETAIL_AMT
                        ,0 as GM
                    from {temp_temp_rfid_inv_boh}
                    where rnk>1) A)
        ;"""
    return SQL


def temp_rfid_room_sales_inventory_wrk_load_sql(batch, load_date, chain_details):
    V_CHAIN = chain_details.get('V_CHAIN')
    if V_CHAIN == constants.V_CHAIN_PFS:
        temp_temp_rfid_sls_inv = f'{schemas.table["tmp"]["tmp_rfid_sls_inv_pfs"]}_{batch}'
        temp_temp_rfid_room_sales_inventory_wrk = f'{schemas.table["tmp"]["tmp_rfid_room_sales_inventory_wrk_pfs"]}_{batch}'
    elif V_CHAIN == constants.V_CHAIN_RLS:
        temp_temp_rfid_sls_inv = f'{schemas.table["tmp"]["tmp_rfid_sls_inv_rls"]}_{batch}'
        temp_temp_rfid_room_sales_inventory_wrk = f'{schemas.table["tmp"]["tmp_rfid_room_sales_inventory_wrk_rls"]}_{batch}'
    SQL = f"""
        insert into {temp_temp_rfid_room_sales_inventory_wrk}                    
        (SELECT CHAIN_ID
                ,GLOBAL_FACILITY_KEY
                ,LOAD_DATE
                ,REGIONAL_PRODUCT_KEY
                ,LOCATION
                ,INVENTORY_QTY
                ,SALES_QTY
                ,SALESCOST_AMT
                ,SALESRETAIL_AMT
            FROM {temp_temp_rfid_sls_inv})
        ;"""
    return SQL


def temp_rfid_room_sales_inventory_load_sql(batch, load_date, chain_details):
    V_CHAIN = chain_details.get('V_CHAIN')
    chain_id = chain_details.get('chain_id')
    lu_d_rfid_store_tgt_cpty = f'{schemas.table["lu"]["d_rfid_store_tgt_cpty"]}'
    lu_d_global_facility = f'{schemas.table["lu"]["d_global_facility"]}'
    lu_d_facility_currency = f'{schemas.table["lu"]["d_facility_currency"]}'
    if V_CHAIN == constants.V_CHAIN_PFS:
        temp_temp_rfid_room_sales_inventory_wrk = f'{schemas.table["tmp"]["tmp_rfid_room_sales_inventory_wrk_pfs"]}_{batch}'
        temp_temp_rfid_room_sales_inventory = f'{schemas.table["tmp"]["tmp_rfid_room_sales_inventory_pfs"]}_{batch}'
    elif V_CHAIN == constants.V_CHAIN_RLS:
        temp_temp_rfid_room_sales_inventory_wrk = f'{schemas.table["tmp"]["tmp_rfid_room_sales_inventory_wrk_rls"]}_{batch}'
        temp_temp_rfid_room_sales_inventory = f'{schemas.table["tmp"]["tmp_rfid_room_sales_inventory_rls"]}_{batch}'
    SQL = f"""
        insert into {temp_temp_rfid_room_sales_inventory}                    
        (SELECT CAST(R.CHAIN_ID AS INT) chain_id
                ,CAST(R.GLOBAL_FACILITY_KEY AS BIGINT) global_facility_key
                ,CAST(REGIONAL_PRODUCT_KEY AS BIGINT) regional_product_key
                ,CAST(LOAD_DATE AS DATE) load_date
                ,R.LOCATION 
                ,CAST(INVENTORY_QTY AS INT) inventory_qty
                ,CAST(SALES_QTY AS INT) sales_qty
                ,CAST(SALESCOST_AMT AS DECIMAL(18,4)) salescost_amt
                ,CAST(SALESRETAIL_AMT AS DECIMAL(18,4)) salesretail_amt
                ,CAST(NVL(SALESRETAIL_AMT-SALESCOST_AMT,0) AS DECIMAL(18,4)) gm
                ,CAST(NVL(T.TGT_CPTY,0) AS INT) tgt_cpty
                ,CAST(NVL(T.SQFT,0) AS INT) sqft
                ,CAST(NVL(C.LOCAL_LEVEL_CURRENCY_ID,1003) AS INT) currency_id
                ,CURRENT_TIMESTAMP AS create_ts
            FROM {temp_temp_rfid_room_sales_inventory_wrk}  R
            LEFT JOIN {lu_d_rfid_store_tgt_cpty} T
                ON R.GLOBAL_FACILITY_KEY=T.GLOBAL_FACILITY_KEY
                AND UPPER(R.LOCATION)=UPPER(T.LOCATION)
            LEFT JOIN {lu_d_global_facility} FACILITY
                ON r.GLOBAL_FACILITY_KEY=FACILITY.GLOBAL_FACILITY_KEY
            LEFT JOIN {lu_d_facility_currency} C
                ON C.facility_id=FACILITY.rtl_facility_cd
            WHERE R.CHAIN_ID={chain_id})
        ;""".format(chain_id=chain_id)
    return SQL


def tgt_rfid_room_sales_inventory_load_sql(batch, load_date, chain_details):
    V_CHAIN = chain_details.get('V_CHAIN')
    chain_id = chain_details.get('chain_id')
    lu_d_rfid_store_inventory = f'{schemas.table["lu"]["d_rfid_store_inventory"]}'
    if V_CHAIN == constants.V_CHAIN_PFS:
        temp_temp_rfid_room_sales_inventory = f'{schemas.table["tmp"]["tmp_rfid_room_sales_inventory_pfs"]}_{batch}'
        tgt_rfid_room_sales_inventory = f'{schemas.table["tgt"]["rfid_room_sales_inventory"]}'
    elif V_CHAIN == constants.V_CHAIN_RLS:
        temp_temp_rfid_room_sales_inventory = f'{schemas.table["tmp"]["tmp_rfid_room_sales_inventory_rls"]}_{batch}'
        tgt_rfid_room_sales_inventory = f'{schemas.table["tgt"]["rfid_room_sales_inventory"]}'
    SQL = f"""
        insert into {tgt_rfid_room_sales_inventory}                           
        (SELECT E.chain_id
                ,E.global_facility_key
                ,E.regional_product_key
                ,E.load_date
                ,COALESCE(D.location,E.location) as location
                ,E.inventory_qty
                ,E.sales_qty
                ,E.salescost_amt
                ,E.salesretail_amt
                ,E.gm
                ,E.tgt_cpty
                ,E.sqft
                ,E.currency_id
                ,E.create_ts
                ,CASE WHEN CAST(E.CHAIN_ID AS INT)=3 THEN 2
                    WHEN CAST(E.CHAIN_ID AS INT)=5 THEN 1
                    WHEN CAST(E.CHAIN_ID AS INT)=1 THEN 3 ELSE -1 END AS source_id
            from {temp_temp_rfid_room_sales_inventory} E
            LEFT JOIN (select distinct global_facility_key
                    ,regional_product_key
                    ,location
                from (select global_facility_key
                            ,regional_product_key
                            ,location
                            ,load_date
                            ,inventory_qty
                            ,rank() over(partition by global_facility_key,regional_product_key,load_date order by inventory_qty desc,location desc) rnk1
                        from (select global_facility_key
                                    ,regional_product_key
                                    ,location
                                    ,load_date
                                    ,inventory_qty
                                    ,rank() over(partition by global_facility_key,regional_product_key order by load_date desc) as rnk
                                from {lu_d_rfid_store_inventory} RFID_STORE_INVENTORY
                                where upper(location) like '%FLOOR%'
                                    and CREATE_TS BETWEEN DATEADD(day,-7,CURRENT_DATE) AND CURRENT_DATE
                                    and exists ( select 1
                                            from {temp_temp_rfid_room_sales_inventory} rfid_room_sales_inventory
                                            where rfid_room_sales_inventory.regional_product_key=RFID_Store_INVENTORY.regional_product_key
                                                and rfid_room_sales_inventory.global_facility_key=RFID_Store_INVENTORY.global_facility_key
                                                and CREATE_TS=current_date
                                                and location='NON-RFID'
                                                and CHAIN_ID={chain_id})) B
                        where rnk=1) C
                where rnk1=1)D
            ON E.global_facility_key=D.global_facility_key
                and E.regional_product_key=D.regional_product_key
                and e.create_ts=current_date
                and e.location='NON-RFID')
        ;""".format(chain_id=chain_id)
    return SQL


def pre_stage_clear_landing_parquet(batch, response_type):
    if response_type == 'Inventory':
        temp_stg_f_store_inventory_parquet = f'{schemas.table["stg_prefix"]["f_store_inventory_parquet"]}_{batch}'
        stg_landing_f_store_inventory_parquet = f"{schemas.table['stage_landing']['f_store_inventory_parquet']}"
        SQL = f"""
             DELETE FROM {stg_landing_f_store_inventory_parquet}
        WHERE
        (
          ("site_name" || '_' ||"zone_name" || '_' ||"product_code" || '_' ||"prodcount" || '_' ||"size" || '_' ||"department" || '_' ||"sku" || '_' ||"season" || '_' ||"color" || '_' ||"Product_name" || '_' ||"LIN" || '_' ||"vendor_style" || '_' ||"Division" || '_' ||"date_updated") IN  (
          SELECT ("site_name" || '_' ||"zone_name" || '_' ||"product_code" || '_' ||"prodcount" || '_' ||"size" || '_' ||"department" || '_' ||"sku" || '_' ||"season" || '_' ||"color" || '_' ||"Product_name" || '_' ||"LIN" || '_' ||"vendor_style" || '_' ||"Division" || '_' ||"date_updated") FROM {temp_stg_f_store_inventory_parquet})
        );
        """
    return SQL


def pre_stage_filter_unload_lookup_failed_items_customer_demographic(batch, s3_bucket, s3_prefix):
    """
        batch : current batch to infer the temp stage data parquet table
        s3_bucket : the bucket which should contain lookup failed items
        s3_prefix : prefix to add to lookup failed items (to identify batch/parquet file)
    """
    d_global_facility = schemas.table['lu']['d_global_facility']
    temp_stg_f_store_traffic_customer_demographic_parquet = f'{schemas.table["stg_prefix"]["f_store_traffic_customer_demographics_parquet"]}_{batch}'

    SQL = f"""
        -- UNLOAD LOOKUP FAILURES
        UNLOAD (
            $$
            SELECT * FROM {temp_stg_f_store_traffic_customer_demographic_parquet} STG
            WHERE
            (
                (CAST(STG."properties_id" AS integer) NOT IN (SELECT DISTINCT RTL_FACILITY_CD   FROM {d_global_facility}))
            ) 
            $$
        )
        TO 's3://{s3_bucket}/{s3_prefix}'
        IAM_ROLE '{iam_role}'
        FORMAT AS PARQUET;
        """
    return SQL


def pre_stage_filter_unload_lookup_failed_items_object_counting(batch, s3_bucket, s3_prefix):
    """
        batch : current batch to infer the temp stage data parquet table
        s3_bucket : the bucket which should contain lookup failed items
        s3_prefix : prefix to add to lookup failed items (to identify batch/parquet file)
    """
    d_global_facility = schemas.table['lu']['d_global_facility']
    temp_stg_f_store_traffic_object_counting_parquet = f'{schemas.table["stg_prefix"]["f_store_traffic_object_counting_parquet"]}_{batch}'

    SQL = f"""
        -- UNLOAD LOOKUP FAILURES
        UNLOAD (
            $$
            SELECT * FROM {temp_stg_f_store_traffic_object_counting_parquet} STG
            WHERE
            (
                (CAST(STG."properties_id" AS integer) NOT IN (SELECT DISTINCT RTL_FACILITY_CD   FROM {d_global_facility}))
            ) 
            $$
        )
        TO 's3://{s3_bucket}/{s3_prefix}'
        IAM_ROLE '{iam_role}'
        FORMAT AS PARQUET;
        """
    return SQL


def transact_sql(*sqls):
    """
      n number of sqls to merge into a single transaction
      eg. transact_sql(sql1, sql2)
    """
    for sql in sqls:
        # sql validations
        if not isinstance(sql, str):
            raise Exception("SQL must be a str")
        if not sql.rstrip().endswith(';'):
            raise Exception("SQL must terminate with semicolon(';') ")

    # combine
    combined_sql = '\n\n'.join(sqls)
    SQL = f"""
    -- BEGIN TRANSACTION
    BEGIN;

    {combined_sql}

    COMMIT;
    -- END TRANSACTION
    END;
    """
    return SQL


def transact_stage_clean_sql(batch, response_type):
    """
        Delete all temporary tables and other clean operations
    """
    SQL = f"""
    -- BEGIN TRANSACTION
    BEGIN;

    {stage_delete_tables_sql(batch, response_type)}
    
    -- END TRANSACTION
    END;
    """
    return SQL


def transact_tmp_clean_sql(batch):
    """
        Delete all temporary tables and other clean operations
    """
    tmp_table_list = schemas.table['tmp']
    SQL_INPUT = ""
    for ket,value in tmp_table_list.items():
        SQL_INPUT += "DROP TABLE IF EXISTS {value}_{batch};".format(value=value,batch=batch)
    SQL = f"""
    -- BEGIN TRANSACTION
    BEGIN;
        {SQL_INPUT}
    -- END TRANSACTION
    END;
    """
    return SQL


def transact_tmp_clean_demographic_dly_sql(batch):
    """
        Delete all temporary tables and other clean operations
    """
    SQL = f"""
    -- BEGIN TRANSACTION
    BEGIN;

    {tmp_delete_demographic_hour_dly_sql(batch)}
    
    -- END TRANSACTION
    END;
    """
    return SQL


def stage_create_tables_sql(batch, response_type):

    if response_type == 'Object_Counting':
        # proto tables
        proto_stg_f_store_traffic_object_counting = schemas.table[
            'stg_proto']['f_store_traffic_object_counting_parquet']

        # temporary tables
        temp_stg_f_store_traffic_object_counting = f"{schemas.table['stg_prefix']['f_store_traffic_object_counting_parquet']}_{batch}"

        # create table sql
        SQL = f"""
        -- CREATE STAGE TABLES
        CREATE TABLE IF NOT EXISTS {temp_stg_f_store_traffic_object_counting} (LIKE {proto_stg_f_store_traffic_object_counting});
        """
    elif response_type == 'Customer_Demographics':
        # proto tables
        proto_stg_f_store_traffic_customer_demographics = schemas.table[
            'stg_proto']['f_store_traffic_customer_demographics_parquet']

        # temporary tables
        temp_stg_f_store_traffic_customer_demographics = f"{schemas.table['stg_prefix']['f_store_traffic_customer_demographics_parquet']}_{batch}"

        # create table sql
        SQL = f"""
        -- CREATE STAGE TABLES
        CREATE TABLE IF NOT EXISTS {temp_stg_f_store_traffic_customer_demographics} (LIKE {proto_stg_f_store_traffic_customer_demographics});
        """
    return SQL


def stage_delete_tables_sql(batch, response_type):
    if response_type == 'Inventory':
        # temporary tables
        temp_stg_f_store_inventory_parquet = f'{schemas.table["stg_prefix"]["f_store_inventory_parquet"]}_{batch}'

        SQL = f"""
        -- DELETE STAGE TABLES
        DROP TABLE IF EXISTS {temp_stg_f_store_inventory_parquet};
        """
    # elif response_type == 'Customer_Demographics':
    #     # temporary tables
    #     temp_stg_f_store_traffic_customer_demographics_parquet = f'{schemas.table["stg_prefix"]["f_store_traffic_customer_demographics_parquet"]}_{batch}'

    #     SQL = f"""
    #     -- DELETE STAGE TABLES
    #     DROP TABLE IF EXISTS {temp_stg_f_store_traffic_customer_demographics_parquet};
    
    return SQL


def tmp_delete_tables_sql(batch):
    # temporary tables
    tmp_f_store_traffic_hour = f'{schemas.table["tmp"]["f_store_traffic_hour"]}_{batch}'

    SQL = f"""
    -- DELETE STAGE TABLES
    DROP TABLE IF EXISTS {tmp_f_store_traffic_hour};
    """
    return SQL


def tmp_delete_demographic_hour_dly_sql(batch):
    # temporary tables
    tmp_f_store_demographic_hour_dly = f'{schemas.table["tmp"]["f_store_demographic_hour_dly"]}_{batch}'

    SQL = f"""
    -- DELETE STAGE TABLES
    DROP TABLE IF EXISTS {tmp_f_store_demographic_hour_dly};
    """
    return SQL


def temp_insert_f_store_traffic_hour_sql(batch):

    tmp_f_store_traffic_hour = f'{schemas.table["tmp"]["f_store_traffic_hour"]}_{batch}'
    temp_stg_f_store_traffic_object_counting_parquet = f'{schemas.table["stg_prefix"]["f_store_traffic_object_counting_parquet"]}_{batch}'
    temp_stg_f_store_traffic_customer_demographics_parquet = f'{schemas.table["stg_prefix"]["f_store_traffic_customer_demographics_parquet"]}_{batch}'

    d_global_facility = schemas.table['lu']['d_global_facility']

    SQL = f"""
    INSERT INTO {tmp_f_store_traffic_hour}
    SELECT 
        NVL(GLOBAL_FACILITY.GLOBAL_FACILITY_KEY, -1)::INTEGER				     AS GLOBAL_FACILITY_KEY
        ,STGOC.properties_id::INTEGER                                            AS RTL_FACILITY_CD
        ,STGOC.properties_scopes_name                                            AS SCOPE_NAME
        ,STGOC.properties_scopes_spaces_spaceid                                  AS SPACE_ID
        ,case when STGOC.properties_scopes_spaces_metrics_datetime is not null
            then 
                timezone(right(STGOC.properties_scopes_spaces_metrics_datetime,3),TO_TIMESTAMP(REPLACE(LEFT(STGOC.properties_scopes_spaces_metrics_datetime,19),'T',''),'YYYY-MM-DDHH24:MI:SS'))
            else
                null
            end                                                                  AS TRACK_DATE
        ,STGOC.properties_scopes_spaces_metrics_timestamp                        AS TRACK_TIME
        ,case when STGOC.properties_scopes_spaces_metrics_datetime is not null
            then right(STGOC.properties_scopes_spaces_metrics_datetime,3)
            else null
            end                                                                  AS TIMEZONE_STR
        ,STGOC.properties_scopes_spaces_name                                     AS SPACE_NAME
        ,STGOC.properties_scopes_spaces_metrics_enter::NUMERIC(18,8)             AS SHOPPER_IN
        ,STGOC.properties_scopes_spaces_metrics_exit::NUMERIC(18,8)              AS SHOPPER_OUT
        ,STGOC.properties_scopes_spaces_metrics_avgoccu::NUMERIC(18,8)           AS AVG_OCCUP
        ,STGOC.properties_scopes_spaces_metrics_maxoccu::NUMERIC(18,8)           AS MAX_OCCUP
        ,STGOC.properties_scopes_spaces_metrics_minoccu::NUMERIC(18,8)           AS MIN_OOCUP
        ,STGOC.properties_scopes_spaces_metrics_endoccu::NUMERIC(18,8)           AS END_OCCUP
        ,STGCD.properties_scopes_spaces_metrics_male17under::NUMERIC(18,8)       AS MALE_17_UNDER
        ,STGCD.properties_scopes_spaces_metrics_male18_24::NUMERIC(18,8)         AS MALE_18_24
        ,STGCD.properties_scopes_spaces_metrics_male25_34::NUMERIC(18,8)         AS MALE_25_34
        ,STGCD.properties_scopes_spaces_metrics_male35_44::NUMERIC(18,8)         AS MALE_35_44
        ,STGCD.properties_scopes_spaces_metrics_male45_54::NUMERIC(18,8)         AS MALE_45_54
        ,STGCD.properties_scopes_spaces_metrics_male55_64::NUMERIC(18,8)         AS MALE_55_64
        ,STGCD.properties_scopes_spaces_metrics_male65over::NUMERIC(18,8)        AS MALE_65_OVER
        ,STGCD.properties_scopes_spaces_metrics_female17under::NUMERIC(18,8)     AS FEMALE_17_UNDER
        ,STGCD.properties_scopes_spaces_metrics_female18_24::NUMERIC(18,8)       AS FEMALE_18_24
        ,STGCD.properties_scopes_spaces_metrics_female25_34::NUMERIC(18,8)       AS FEMALE_25_34
        ,STGCD.properties_scopes_spaces_metrics_female35_44::NUMERIC(18,8)       AS FEMALE_35_44
        ,STGCD.properties_scopes_spaces_metrics_female45_54::NUMERIC(18,8)       AS FEMALE_45_54
        ,STGCD.properties_scopes_spaces_metrics_female55_64::NUMERIC(18,8)       AS FEMALE_55_64
        ,STGCD.properties_scopes_spaces_metrics_female65over::NUMERIC(18,8)      AS FEMALE_65_OVER
        ,'{batch}'                                                               AS CREATEBATCH_ID
        ,'{batch}'                                                               AS UPDATEBATCH_ID
    FROM {temp_stg_f_store_traffic_object_counting_parquet} STGOC
    LEFT JOIN {temp_stg_f_store_traffic_customer_demographics_parquet} STGCD
        ON   STGOC.properties_id = STGCD.properties_id
        AND  STGOC.properties_scopes_name = STGCD.properties_scopes_name
        AND  STGOC.properties_scopes_spaces_spaceId = STGCD.properties_scopes_spaces_spaceId
        AND  substring(STGOC.properties_scopes_spaces_metrics_datetime,1,13) =  substring(STGCD.properties_scopes_spaces_metrics_datetime,1,13)
        AND  STGOC.properties_scopes_spaces_name = STGCD.properties_scopes_spaces_name
    LEFT JOIN (select RTL_FACILITY_CD,GLOBAL_FACILITY_KEY from {d_global_facility} where RTL_FACILITY_CD is not null) GLOBAL_FACILITY
        ON   STGOC.properties_id::INTEGER = GLOBAL_FACILITY.RTL_FACILITY_CD
    ;
    """
    return SQL


def target_delete_f_store_traffic_hour_sql(batch):

    tmp_f_store_traffic_hour = f'{schemas.table["tmp"]["f_store_traffic_hour"]}_{batch}'
    tgt_f_store_traffic_hour = schemas.table['tgt']['f_store_traffic_hour']

    SQL = f"""
    DELETE FROM {tgt_f_store_traffic_hour}
    WHERE
        (
          (RTL_FACILITY_CD || SCOPE_NAME || SPACE_ID || to_char(TRACK_DATE,'YYYY-MM-DDHH') || SPACE_NAME || TIMEZONE_STR) IN (
          SELECT (RTL_FACILITY_CD || SCOPE_NAME || SPACE_ID || to_char(TRACK_DATE,'YYYY-MM-DDHH') || SPACE_NAME || TIMEZONE_STR)
          FROM {tmp_f_store_traffic_hour})
        );
    """
    return SQL


def target_insert_f_store_traffic_hour_sql(batch):

    tgt_f_store_traffic_hour = schemas.table['tgt']['f_store_traffic_hour']
    tmp_f_store_traffic_hour = f'{schemas.table["tmp"]["f_store_traffic_hour"]}_{batch}'

    current_dt = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    SQL = f"""
    INSERT INTO {tgt_f_store_traffic_hour}
    SELECT
        GLOBAL_FACILITY_KEY
        ,RTL_FACILITY_CD
        ,SCOPE_NAME
        ,SPACE_ID
        ,TRACK_DATE
        ,TRACK_TIME
        ,TIMEZONE_STR
        ,SPACE_NAME
        ,SHOPPER_IN
        ,SHOPPER_OUT
        ,AVG_OCCUP
        ,MAX_OCCUP
        ,MIN_OOCUP
        ,END_OCCUP
        ,MALE_17_UNDER
        ,MALE_18_24
        ,MALE_25_34
        ,MALE_35_44
        ,MALE_45_54
        ,MALE_55_64
        ,MALE_65_OVER
        ,FEMALE_17_UNDER
        ,FEMALE_18_24
        ,FEMALE_25_34
        ,FEMALE_35_44
        ,FEMALE_45_54
        ,FEMALE_55_64
        ,FEMALE_65_OVER
        ,CREATEBATCH_ID
        ,UPDATEBATCH_ID
        ,to_timestamp('{current_dt}', 'YYYY-MM-DD HH24:MI:SS')                   AS CREATE_TS
        ,to_timestamp('{current_dt}', 'YYYY-MM-DD HH24:MI:SS')                   AS UPDATE_TS
    FROM {tmp_f_store_traffic_hour} TMP
    ;
    """
    return SQL


def target_delete_traffic_counter_detail_sql(batch):

    tmp_f_store_traffic_hour = f'{schemas.table["tmp"]["f_store_traffic_hour"]}_{batch}'
    tgt_traffic_counter_detail = schemas.table['tgt']['traffic_counter_detail']
    lu_d_edw_source = f'{schemas.table["lu"]["d_edw_source"]}'

    SQL = f"""
    DELETE FROM {tgt_traffic_counter_detail}
    WHERE
        (
          (GLOBAL_FACILITY_KEY || to_char(TRACK_DATE,'YYYY-MM-DDHH') || SOURCE_ID) IN (
          SELECT (GLOBAL_FACILITY_KEY || to_char(TRACK_DATE,'YYYY-MM-DDHH') || SUBSTRING(track_time,1,2) || (select source_id from {lu_d_edw_source} where source_cd = 'DEEP_NORTH'))
          FROM {tmp_f_store_traffic_hour})
        );
    """
    return SQL


def target_insert_traffic_counter_detail_sql(batch):

    tgt_traffic_counter_detail = schemas.table['tgt']['traffic_counter_detail']
    tmp_f_store_traffic_hour = f'{schemas.table["tmp"]["f_store_traffic_hour"]}_{batch}'
    lu_d_edw_source = f'{schemas.table["lu"]["d_edw_source"]}'

    current_dt = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    SQL = f"""
    INSERT INTO {tgt_traffic_counter_detail}
    SELECT distinct
        global_facility_key
        ,track_date
        ,to_char(track_date,'HH24:MI:SS')
        ,shopper_in
        ,shopper_out
        ,NULL::INTEGER passerby_traffic
        ,(select source_id from {lu_d_edw_source} where source_cd = 'DEEP_NORTH') source_id
        ,NULL::INTEGER createbatch_id
        ,NULL::INTEGER updatebatch_id
        ,to_timestamp('{current_dt}', 'YYYY-MM-DD HH24:MI:SS')                   AS CREATE_TS
        ,to_timestamp('{current_dt}', 'YYYY-MM-DD HH24:MI:SS')                   AS UPDATE_TS
    FROM {tmp_f_store_traffic_hour} TMP
    ;
    """
    return SQL


def temp_insert_f_store_demographic_hour_dly_sql(batch):

    tmp_f_store_demographic_hour_dly = f'{schemas.table["tmp"]["f_store_demographic_hour_dly"]}_{batch}'
    tmp_f_store_traffic_hour = f'{schemas.table["tmp"]["f_store_traffic_hour"]}_{batch}'

    SQL = f"""
    INSERT INTO {tmp_f_store_demographic_hour_dly}
    SELECT
    GLOBAL_FACILITY_KEY,
    RTL_FACILITY_CD,
    SCOPE_NAME,
    SPACE_ID,
    TRUNC(TRACK_DATE) TRACK_DT,
    EXTRACT(HOUR FROM TRACK_DATE) TRACK_HR,
    GENDER,
    AGE_GROUP,
    SPACE_NAME,
    SHOPPER_IN
    from
    (
    SELECT GLOBAL_FACILITY_KEY, RTL_FACILITY_CD, SCOPE_NAME, SPACE_ID, TRACK_DATE, 'Male' AS GENDER, 'under17' AS AGE_GROUP, SPACE_NAME, MALE_17_UNDER AS SHOPPER_IN,'{batch}' AS CREATEBATCH_ID ,'{batch}' AS UPDATEBATCH_ID FROM {tmp_f_store_traffic_hour}
    UNION ALL
     SELECT GLOBAL_FACILITY_KEY, RTL_FACILITY_CD, SCOPE_NAME, SPACE_ID, TRACK_DATE, 'Male' AS GENDER, '18-24' AS AGE_GROUP, SPACE_NAME, MALE_18_24 AS SHOPPER_IN,'{batch}' AS CREATEBATCH_ID ,'{batch}' AS UPDATEBATCH_ID FROM {tmp_f_store_traffic_hour}
     UNION ALL
     SELECT GLOBAL_FACILITY_KEY, RTL_FACILITY_CD, SCOPE_NAME, SPACE_ID, TRACK_DATE, 'Male' AS GENDER, '25-34' AS AGE_GROUP, SPACE_NAME, MALE_25_34 AS SHOPPER_IN,'{batch}' AS CREATEBATCH_ID ,'{batch}' AS UPDATEBATCH_ID FROM {tmp_f_store_traffic_hour}
     UNION ALL
     SELECT GLOBAL_FACILITY_KEY, RTL_FACILITY_CD, SCOPE_NAME, SPACE_ID, TRACK_DATE, 'Male' AS GENDER, '35-44' AS AGE_GROUP, SPACE_NAME, MALE_35_44 AS SHOPPER_IN,'{batch}' AS CREATEBATCH_ID ,'{batch}' AS UPDATEBATCH_ID FROM {tmp_f_store_traffic_hour}
     UNION ALL
     SELECT GLOBAL_FACILITY_KEY, RTL_FACILITY_CD, SCOPE_NAME, SPACE_ID, TRACK_DATE, 'Male' AS GENDER, '45-54' AS AGE_GROUP, SPACE_NAME, MALE_45_54 AS SHOPPER_IN,'{batch}' AS CREATEBATCH_ID ,'{batch}' AS UPDATEBATCH_ID FROM {tmp_f_store_traffic_hour}
     UNION ALL
     SELECT GLOBAL_FACILITY_KEY, RTL_FACILITY_CD, SCOPE_NAME, SPACE_ID, TRACK_DATE, 'Male' AS GENDER, '55-64' AS AGE_GROUP, SPACE_NAME, MALE_55_64 AS SHOPPER_IN,'{batch}' AS CREATEBATCH_ID ,'{batch}' AS UPDATEBATCH_ID FROM {tmp_f_store_traffic_hour}
     UNION ALL
     SELECT GLOBAL_FACILITY_KEY, RTL_FACILITY_CD, SCOPE_NAME, SPACE_ID, TRACK_DATE, 'Male' AS GENDER, 'over 65' AS AGE_GROUP, SPACE_NAME, MALE_65_OVER AS SHOPPER_IN,'{batch}' AS CREATEBATCH_ID ,'{batch}' AS UPDATEBATCH_ID FROM {tmp_f_store_traffic_hour}
     UNION ALL
     SELECT GLOBAL_FACILITY_KEY, RTL_FACILITY_CD, SCOPE_NAME, SPACE_ID, TRACK_DATE, 'Female' AS GENDER, 'under17' AS AGE_GROUP, SPACE_NAME, FEMALE_17_UNDER AS SHOPPER_IN,'{batch}' AS CREATEBATCH_ID ,'{batch}' AS UPDATEBATCH_ID FROM {tmp_f_store_traffic_hour}
     UNION ALL
     SELECT GLOBAL_FACILITY_KEY, RTL_FACILITY_CD, SCOPE_NAME, SPACE_ID, TRACK_DATE, 'Female' AS GENDER, '18-24' AS AGE_GROUP, SPACE_NAME, FEMALE_18_24 AS SHOPPER_IN,'{batch}' AS CREATEBATCH_ID ,'{batch}' AS UPDATEBATCH_ID FROM {tmp_f_store_traffic_hour}
     UNION ALL
     SELECT GLOBAL_FACILITY_KEY, RTL_FACILITY_CD, SCOPE_NAME, SPACE_ID, TRACK_DATE, 'Female' AS GENDER, '25-34' AS AGE_GROUP, SPACE_NAME, FEMALE_25_34 AS SHOPPER_IN,'{batch}' AS CREATEBATCH_ID ,'{batch}' AS UPDATEBATCH_ID FROM {tmp_f_store_traffic_hour}
     UNION ALL
     SELECT GLOBAL_FACILITY_KEY, RTL_FACILITY_CD, SCOPE_NAME, SPACE_ID, TRACK_DATE, 'Female' AS GENDER, '35-44' AS AGE_GROUP, SPACE_NAME, FEMALE_35_44 AS SHOPPER_IN,'{batch}' AS CREATEBATCH_ID ,'{batch}' AS UPDATEBATCH_ID FROM {tmp_f_store_traffic_hour}
     UNION ALL
     SELECT GLOBAL_FACILITY_KEY, RTL_FACILITY_CD, SCOPE_NAME, SPACE_ID, TRACK_DATE, 'Female' AS GENDER, '45-54' AS AGE_GROUP, SPACE_NAME, FEMALE_45_54 AS SHOPPER_IN,'{batch}' AS CREATEBATCH_ID ,'{batch}' AS UPDATEBATCH_ID FROM {tmp_f_store_traffic_hour}
     UNION ALL
     SELECT GLOBAL_FACILITY_KEY, RTL_FACILITY_CD, SCOPE_NAME, SPACE_ID, TRACK_DATE, 'Female' AS GENDER,'55-64' AS AGE_GROUP, SPACE_NAME, FEMALE_55_64 AS SHOPPER_IN,'{batch}' AS CREATEBATCH_ID ,'{batch}' AS UPDATEBATCH_ID FROM {tmp_f_store_traffic_hour}
     UNION ALL
     SELECT GLOBAL_FACILITY_KEY, RTL_FACILITY_CD, SCOPE_NAME, SPACE_ID, TRACK_DATE, 'Female' AS GENDER, 'over 65' AS AGE_GROUP, SPACE_NAME, FEMALE_65_OVER AS SHOPPER_IN,'{batch}' AS CREATEBATCH_ID ,'{batch}' AS UPDATEBATCH_ID FROM {tmp_f_store_traffic_hour}
    )
    ;
    """
    return SQL


def target_delete_f_store_demographic_hour_dly_sql(batch):

    tmp_f_store_demographic_hour_dly = f'{schemas.table["tmp"]["f_store_demographic_hour_dly"]}_{batch}'
    tgt_f_store_demographic_hour_dly = schemas.table['tgt']['f_store_demographic_hour_dly']

    SQL = f"""
    DELETE FROM {tgt_f_store_demographic_hour_dly}
    WHERE
        (
          (RTL_FACILITY_CD || SCOPE_NAME || SPACE_ID || to_char(TRACK_DT,'YYYY-MM-DD') || TRACK_HR || GENDER || AGE_GROUP || SPACE_NAME) IN (
          SELECT (RTL_FACILITY_CD || SCOPE_NAME || SPACE_ID || to_char(TRACK_DT,'YYYY-MM-DD') || TRACK_HR || GENDER || AGE_GROUP || SPACE_NAME)
          FROM {tmp_f_store_demographic_hour_dly})
        );
    """
    return SQL


def target_insert_f_store_demographic_hour_dly_sql(batch):

    tmp_f_store_demographic_hour_dly = f'{schemas.table["tmp"]["f_store_demographic_hour_dly"]}_{batch}'
    tgt_f_store_demographic_hour_dly = schemas.table['tgt']['f_store_demographic_hour_dly']

    current_dt = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    SQL = f"""
    INSERT INTO {tgt_f_store_demographic_hour_dly}
    SELECT distinct
        global_facility_key
        ,rtl_facility_cd
        ,scope_name
        ,space_id
        ,track_dt
        ,track_hr
        ,gender
        ,age_group
        ,space_name
        ,shopper_in
        ,CREATEBATCH_ID
        ,UPDATEBATCH_ID
        ,to_timestamp('{current_dt}', 'YYYY-MM-DD HH24:MI:SS')                   AS CREATE_TS
        ,to_timestamp('{current_dt}', 'YYYY-MM-DD HH24:MI:SS')                   AS UPDATE_TS
    FROM {tmp_f_store_demographic_hour_dly} TMP
    ;
    """
    return SQL
