import time
import boto3
import botocore
import logging
import json
import datetime

from utils import redshift
from utils import notifications
from etl import sqls
import constants

# CLIENTS
s3_client = boto3.client('s3')

# LOGGING
logger = logging.getLogger('etl')
logger.setLevel(logging.DEBUG)
# formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# handler = logging.StreamHandler()
# handler.setFormatter(formatter)
# handler.setLevel(logging.DEBUG)
# logger.addHandler(handler)


def get_chain_details(V_CHAIN):
    try:
        if V_CHAIN == 'PFS':
            wh = '%WARE%'
            bh = '%HOUSE%'
            floor = '%FLOOR%'
            chain_id = 3
            source_id = 2

        elif V_CHAIN == 'RLS':
            wh = '%NA%'
            bh = '%STOCK%'
            floor = '%ZONE%'
            chain_id = 5
            source_id = 1
        return {'V_CHAIN': V_CHAIN, 'wh': wh, 'bh': bh, 'floor': floor, 'chain_id': chain_id, 'source_id': source_id}
    except:
        logger.error("Incorrect chain")


def handle_hard_error(bucket, key):
    """
        bucket : bucket containing the hard error file
        key : object key for the hard error file
    """
    s3_path = f's3://{bucket}/{key}'

    # move to hard error location
    filename = key.split('/')[-1]
    error_bucket = constants.HARD_ERROR_BUCKET
    error_key = f"{constants.HARD_ERROR_PREFIX}{filename}"
    try:
        s3_client.copy({'Bucket': bucket, 'Key': key}, error_bucket, error_key)
        # s3_client.delete_object(Bucket=bucket, Key=key)   # do not delete from source
        logger.error(f"Moved {s3_path} to s3://{error_bucket}/{error_key}")
    except:
        logger.error(f"Failed to Move File to s3://{error_bucket}/{error_key}")


def etl_landing_load(bucket, key, retry=True):
    """
        Load incoming parquet landing
    """
    s3_path = f's3://{bucket}/{key}'
    try:
        logger.info(f"Initiating Load for : {s3_path}")

        SQL = sqls.transact_landing_load_sql(s3_path)
        logger.info(f"SQL: {SQL}")
        q = redshift.execute_sql(SQL)
        redshift.wait_for_queries([q])
        logger.info(f"Landing Load completed for {s3_path}")

    except Exception as e:
        logger.exception(f"ERROR : {e.__class__.__name__} : {str(e)}")

        # MAINTENANCE WINDOW EXCEPTION
        if isinstance(e, botocore.exceptions.ClientError):
            if e.response['Error']['Code'] == 'ValidationException' and 'PATCH_OFFLINE' in str(e):
                logger.error(
                    "Validation Exceptions due to maintenance. Notifications Skipped")
                return  # skip notifications

        # Retry once before reporting error
        if retry:
            logger.info("Retrying Landing Load.")
            etl_landing_load(bucket, key, retry=False)
            return

        # HARD ERROR
        handle_hard_error(bucket, key)

        # EXTRACT ERROR DETAILS FOR NOTIFICATION
        error = None
        if isinstance(e, redshift.QueryFailure):
            try:
                # q is expected to be the last id that failed (observe all query ids are named q)
                error_response = redshift.sql_status_details(q)
                if error_response:
                    error_message = error_response.get(
                        'Error', f'Unable to retrieve sql error details for {id}')
                    error_status = error_response.get(
                        'Status', f'Unable to retrieve status.')
                    error = f"Query Execution : {error_status}. Desc : {error_message}"
            except:
                pass

        if not error:
            error = f"Operation Raised Error : {e.__class__.__name__}:{str(e)}"

        # NOTIFICATIONS
        notifications.initiate_error_notification(bucket, key, error)


def etl_load(start_process=None, batch=None, retry=True):
    """
        ETL entrypoint to process S3 file
        Uses landing table to load

        start_process : process(str) from which to start loading (if specified)
                : None specifies that process is started from begining.
        batch : batch name
        retry : Flag indicating whether retry should be attempted upon failure
    """

    ms_timestamp = int(time.time() * 1000)
    if batch is None:
        batch = str(ms_timestamp)

    if start_process is None:
        run_query = True
    else:
        run_query = False

    process = "STAGE_INITIATE"
    run_query = run_query or process == start_process
    if run_query:
        logger.info(process)

    try:
        # INVENTORY LANDING TABLE COUNT CHECK ============================================
        process = "INVENTORY_LANDING_COUNT_CHECK"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.landing_count_sql('Inventory')
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])
            count = redshift.get_results(q)[0][0]
            logger.debug(f"Landing table count : {count}")
            if int(count) <= 0:
                logger.info("Landing table empty. Skipping load")
                return  # Note that this return skips execution and jumps to finally clause

        # Load Inventory pre-stage tables ============================================
        process = "INVENTORY_PRE_STAGE_TABLE_LOAD"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            lu_error_bucket = constants.SOFT_ERROR_BUCKET
            lu_error_prefix = f'{constants.LU_ERROR_PREFIX}{batch}/{ms_timestamp}/'
            SQL = sqls.transact_pre_stage_load_landing_sql(
                batch, lu_error_bucket, lu_error_prefix, 'Inventory')
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        # TEMP_LOAD ===================================================
        process = "TEMP_LOAD_SQ_INV"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_sq_inv_load_sql(batch)
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_SCAN_CODE_MODIFIED"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_scan_code_modified_load_sql(batch)
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_EXP_STORE_VALIDATION"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_exp_store_validation_load_sql(batch)
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RTR_STORE_MAP_EXCEPTION"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rtr_store_map_exception_load_sql(batch)
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RFID_STORE_INVENTORY_STG"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rfid_store_inventory_stg_load_sql(batch)
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RFID_EXCEPTION"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rfid_exception_load_sql(batch)
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RFID_MISSING_INVENTORY_STG"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rfid_missing_inventory_stg_load_sql(batch)
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_INVENTORY_MERGE"
        LOAD_DATE = datetime.datetime.now().date()-datetime.timedelta(1)
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_inventory_merge_load_sql(batch)
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_ALTER_CALC_INV_QTY_MISSING_INVENTORY_MERGE"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_missing_inventory_merge_alter_CALC_INV_QTY_sql(batch)
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_ALTER_CC_IND_MISSING_INVENTORY_MERGE"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_missing_inventory_merge_alter_CC_IND_sql(batch)
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        # # TARGET LOAD ==========================================================
        process = "TARGET_LOAD_RFID_STORE_INVENTORY"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.tgt_rfid_store_inventory_load_sql(batch, LOAD_DATE)
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_MISSING_INVENTORY_MERGE"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_missing_inventory_merge_load_sql(batch)
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TARGET_UPDATE_120_DAYS_RFID_MISSING_INVENTORY"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.tgt_rfid_missing_inventory_update_120_days_sql(batch)
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TARGET_LOAD_RFID_MISSING_INVENTORY"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.tgt_rfid_missing_inventory_load_sql(batch)
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RFID_LOCATION_XREF_PFS"
        load_date = datetime.datetime.now().date()-datetime.timedelta(1)
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rfid_location_xref_load_sql(
                    batch, load_date, get_chain_details(constants.V_CHAIN_PFS))
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RFID_ROOM_INV_TEMP_PFS"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rfid_room_inv_temp_load_sql(
                    batch, load_date, get_chain_details(constants.V_CHAIN_PFS))
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RFID_INV_NON_RFID_PFS"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rfid_inv_non_rfid_load_sql(
                    batch, load_date, get_chain_details(constants.V_CHAIN_PFS))
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RFID_INV_NON_RFID_LOC_PFS"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rfid_inv_non_rfid_loc_pfs_load_sql(
                    batch, load_date, get_chain_details(constants.V_CHAIN_PFS))
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RFID_INV_BOH_PFS"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rfid_inv_boh_load_sql(
                    batch, load_date, get_chain_details(constants.V_CHAIN_PFS))
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RFID_SLS_INV_PFS"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rfid_sls_inv_load_sql(
                    batch, load_date, get_chain_details(constants.V_CHAIN_PFS))
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RFID_ROOM_SALES_INVENTORY_WRK_PFS"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rfid_room_sales_inventory_wrk_load_sql(
                    batch, load_date, get_chain_details(constants.V_CHAIN_PFS))
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RFID_ROOM_SALES_INVENTORY_PFS"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rfid_room_sales_inventory_load_sql(
                    batch, load_date, get_chain_details(constants.V_CHAIN_PFS))
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TARGET_LOAD_RFID_ROOM_SALES_INVENTORY_PFS"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.tgt_rfid_room_sales_inventory_load_sql(
                    batch, load_date, get_chain_details(constants.V_CHAIN_PFS))
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RFID_LOCATION_XREF_RLS"
        load_date = datetime.datetime.now().date()-datetime.timedelta(1)
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rfid_location_xref_load_sql(
                    batch, load_date, get_chain_details(constants.V_CHAIN_RLS))
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RFID_ROOM_INV_TEMP_RLS"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rfid_room_inv_temp_load_sql(
                    batch, load_date, get_chain_details(constants.V_CHAIN_RLS))
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RFID_INV_NON_RFID_RLS"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rfid_inv_non_rfid_load_sql(
                    batch, load_date, get_chain_details(constants.V_CHAIN_RLS))
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RFID_INV_NON_RFID_LOC_RLS"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rfid_inv_non_rfid_loc_rls_load_sql(
                    batch, load_date, get_chain_details(constants.V_CHAIN_RLS))
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RFID_INV_BOH_RLS"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rfid_inv_boh_load_sql(
                    batch, load_date, get_chain_details(constants.V_CHAIN_RLS))
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RFID_SLS_INV_RLS"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rfid_sls_inv_load_sql(
                    batch, load_date, get_chain_details(constants.V_CHAIN_RLS))
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RFID_ROOM_SALES_INVENTORY_WRK_RLS"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rfid_room_sales_inventory_wrk_load_sql(
                    batch, load_date, get_chain_details(constants.V_CHAIN_RLS))
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TEMP_LOAD_RFID_ROOM_SALES_INVENTORY_RLS"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.temp_rfid_room_sales_inventory_load_sql(
                    batch, load_date, get_chain_details(constants.V_CHAIN_RLS))
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        process = "TARGET_LOAD_RFID_ROOM_SALES_INVENTORY_RLS"
        run_query = run_query or process == start_process
        if run_query:
            logger.info(process)
            SQL = sqls.transact_sql(
                sqls.tgt_rfid_room_sales_inventory_load_sql(
                    batch, load_date, get_chain_details(constants.V_CHAIN_RLS))
            )
            q = redshift.execute_sql(SQL, name=process)
            redshift.wait_for_queries([q])

        # CLEAN INVENTORY STAGE TABLES ===============================================
        process = "INVENTORY_STAGE_CLEAN"
        run_query = run_query or process == start_process
        if run_query :
            logger.info(process)
            SQL = sqls.transact_stage_clean_sql(batch, 'Inventory')
            q = redshift.execute_sql(SQL, process)
            redshift.wait_for_queries([q])

        # CLEAN TEMP TABLES ===============================================
        process = "INVENTORY_TMP_CLEAN"
        run_query = run_query or process == start_process
        if run_query :
            logger.info(process)
            SQL = sqls.transact_tmp_clean_sql(batch)
            q = redshift.execute_sql(SQL, process)
            redshift.wait_for_queries([q])

        logger.info("ETL OPERATION SUCCESS")

    except Exception as e:
        logger.error(f"ETL OPERATION FAILED AT : {process}")
        logger.exception(f"ERROR : {e.__class__.__name__} : {str(e)}")

        # MAINTENANCE WINDOW EXCEPTION
        if isinstance(e, botocore.exceptions.ClientError):
            if e.response['Error']['Code'] == 'ValidationException' and 'PATCH_OFFLINE' in str(e):
                logger.error(
                    "Validation Exceptions due to maintenance. Notifications Skipped")
                return  # skip notifications

        # retry once again before reporting error
        # NOTE : it's important to use the same batch name during retries.
        #       because all table names are identified using batch
        if retry:
            logger.info("Retrying ETL Load.")
            etl_load(start_process=process, batch=batch, retry=False)
            return

        # EXTRACT ERROR DETAILS FOR NOTIFICATION
        error = None
        if isinstance(e, redshift.QueryFailure):
            try:
                # q is expected to be the last id that failed (observe all query ids are named q)
                error_response = redshift.sql_status_details(q)
                if error_response:
                    error_message = error_response.get(
                        'Error', f'Unable to retrieve sql error details for {id}')
                    error_status = error_response.get(
                        'Status', f'Unable to retrieve status.')
                    error = f"Query Execution : {error_status}. Desc : {error_message}"
            except:
                pass

        if not error:
            error = f"Operation Raised Error : {e.__class__.__name__}:{str(e)}"

        # NOTIFICATIONS
        notifications.initiate_error_notification(
            f'BATCH_{batch}', 'NA', error)

    finally:
        pass
