import os
from pprint import pprint
import sys
import boto3
from git import Repo

lambda_client = boto3.client('lambda')
repo = Repo(os.getcwd())
assert not repo.bare

defaults = {
  'lambda' : 'rl-sales-load',
  'branch' : 'dev'
}

if len(sys.argv) == 1 :
    branch = defaults['branch']
    target_lambda = defaults['lambda']
elif len(sys.argv) == 3 :
    branch = sys.argv[1]
    target_lambda = sys.argv[2]
else :
    print("usage : python3 deploy.py [<branch> <lambda>]")
    print(f"default branch => {defaults['branch']}\ndefault lambda => {defaults['lambda']}")
    exit()

tmp_dir = './tmp/'
zip_file = './tmp/code.zip'

# create tmp dir
os.makedirs(tmp_dir, exist_ok=True)

# clear previous zip files if any
if os.path.exists(zip_file) : os.remove(zip_file)

# create zip
with open(zip_file, 'wb') as archive_file : 
    repo.archive(archive_file, treeish=branch, format='zip')

# read zip file
with open(zip_file, 'rb') as f:
    zipped_code = f.read()

# update function code
params = {
  'FunctionName' : target_lambda,
  'ZipFile' : zipped_code
}

try : 
    resp = lambda_client.update_function_code(**params)
    
    if int(resp['ResponseMetadata']['HTTPStatusCode']) == 200 : 
        print(f"Deployed :\nbranch : {branch}\nlambda : {target_lambda}")
    else :
        print(f"Failed to deploy :\nbranch : {branch}\nlambda : {target_lambda}")
        print("Response : ")
        pprint(resp)
except Exception as e : 
    print(f"Failed to deploy :\nbranch : {branch}\nlambda : {target_lambda}")
    print(f"Exception : {e.__class__.__name__} : {str(e)}")

# clear tmp file 
if os.path.exists(zip_file) : os.remove(zip_file)
