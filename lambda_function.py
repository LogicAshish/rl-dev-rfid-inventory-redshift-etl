from etl.load import etl_load, etl_landing_load
from etl.bulk_load import etl_bulk_load
import json
import boto3

lambda_client = boto3.client('lambda')


def lambda_handler(event, context):
    print("Incoming event:")
    eventString = json.dumps(event)
    print(eventString)
    if 'ActionEvent' in event :
        action = event['ActionEvent']
        print(f"Encountered Action Event : {action}")

        # handle bulk processing
        if action == 'bulk_load' :
            batch = event['batch']
            bucket = event['bucket']
            prefix = event['prefix']
            print(f"params :: action : {action}; bucket : {bucket}; prefix : {prefix}")

            etl_bulk_load(bucket, prefix, batch)

        # avoid normal event processing
        return

    # S3 key information
    if 'Records' in event :
        key = event['Records'][0]['s3']['object']['key']
        bucket = event['Records'][0]['s3']['bucket']['name']
        etl_landing_load(bucket, key)
        return

    # Event Bridge Trigger
    if 'detail-type' in event and event['detail-type'] == 'Scheduled Event' :
        etl_load()
        return
    else :
        print("No Action Taken")
